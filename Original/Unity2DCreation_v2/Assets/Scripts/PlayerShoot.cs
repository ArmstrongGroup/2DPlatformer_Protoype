using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Player shoot instantiates bullet projectiles once the player has acquired the gun
public class PlayerShoot : MonoBehaviour
{
    //Public variables
    //Serialized
    [SerializeField]
    GameObject bullet;//Reference to the bullet prefab. Needs to be populated from the assets folder, not the scene
    //Could be a property that is gettable and settable, or hidden in inspector
    public bool hasGun;//Stores the state of the player having the gun or not

    //Private variables
    PlayerController _pController;//Reference to player controller to get inputs
    int _shootDir = 1;//Stores shooting direction
    Vector3 _projectileRot = Vector3.zero;//Stores rotation to set instanced bullet to

    // Start is called before the first frame update
    void Start()
    {
        _pController = GetComponent<PlayerController>();//Populates player controller variable (assumes there is PlayerController component attached to this game object)
    }

    // Update is called once per frame from the PlayerStateController
    public void ShootUpdate()
    {
        if (hasGun)//Assess whether the player has the gun
        {
            UpdateShootDir();//Sets the direction to direction the player is facing
            Shoot();//Creates bullet on player input
        }
    }

    //Assess and sets variables to ensure bullet is shot correcty
    void UpdateShootDir() 
    {
        if (_pController.InputX != 0)//Only called if player is moving
        {
            if (_pController.InputX > 0)//If input is moving right
            {
                _shootDir = 1;//Sets shoot direction to positive
                _projectileRot = Vector3.zero;//Sets bullet rotation to zero in all axis
            }
            else //If input is moving left
            {
                _shootDir = -1;//Sets shoot direction to negative
                _projectileRot = new Vector3(0,0,180);//Sets bullet rotation to 180 in z to flip direction
            }
        }
    }

    //Creates bullets
    void Shoot() 
    {
        if (_pController.Fire)//If the player presses fire 
        {
            //Instantiates a bullet at a position and rotation based upon shoot direction
            Instantiate(bullet, transform.position + new Vector3(0.35f * _shootDir, 0.5f, 0), Quaternion.Euler(_projectileRot));
        }   
    }
}
