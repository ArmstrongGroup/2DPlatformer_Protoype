using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Player controller is possibly named incorrectly 
//This is really an inputs class and should probably be named as such and attached to the game manager object in the editor
public class PlayerController : MonoBehaviour
{
    //Getable variables
    public float InputX { get { return _inputX; } }
    float _inputX = 0;

    public bool Jump { get { return _jump; } }
    bool _jump;

    public bool Fire { get { return _fire; } }
    bool _fire;

    public bool InventoryShow { get { return _invShow; } }
    bool _invShow;

    //Update is called once per frame from the Player State Controller
    //Probably should be called from the Game Manager, this would allow for inputs to be used for more than the player character
    public void InputUpdate()
    {
        //Calls input functions
        GetXInput();
        GetJump();
        GetFire();
        GetInventory();
    }

    //Gets X input from input manager 
    void GetXInput() 
    {
        _inputX = Input.GetAxisRaw("Horizontal");
    }

    //Gets jump input from input manager 
    void GetJump() 
    {
        _jump = Input.GetButtonDown("Jump");
    }

    //Gets fire input from input manager 
    void GetFire() 
    {
        _fire = Input.GetButtonDown("Fire1");
    }

    //Gets inventory input from input manager 
    void GetInventory()
    {
        _invShow = Input.GetKeyDown(KeyCode.I);
        //Should set up an input in the input manager for this
        //This would enable multiple input devices and platforms to be set up simultaneously via the input manager 
    }
}
