using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.
using Cinemachine;//Cinemachine namespace added to enable access to Unity's virtual camera systems.

//Player State Contoller runs different functionality on the player depending what state it is in.
//At present there is only a single state, however, this is other states should be added.
public class PlayerStateController : MonoBehaviour
{
    //References, as the main heart of the player character, this class has a lot of dependancies.
    //This could be improved through depentance inversion.
    PlayerController _pControl;//PlayerControler governs the player input .
    CharacterMovement _cMove;//Character movement moves the character in the x axis.
    CharacterJump _cJump;//CharacterJump applies jumping force.
    CollisionChecks _cChecks;//CollisionChecks evaluates all objects touching the player every frame.
    Rigidbody2D _rb; //Rigidbody2D component places player game object in the physics system.
    Collider2D _col;//Collider enables collisions.
    CinemachineVirtualCamera _vCam;//Virtual camera used to track the player.
    PlayerShoot _pShoot;//PlayerShoot enables the player to fire projectiles.
    GameManager _gManager;//GameManager controls the state of the game.
    
    

    // Start is called before the first frame update
    void Start()
    {
        //Assigning reference variables
        _pControl = GetComponent<PlayerController>();//Finds component attached to this gameobject (assumes its existance)
        _cMove = GetComponent<CharacterMovement>();//Finds component attached to this gameobject (assumes its existance)
        _cJump = GetComponent<CharacterJump>();//Finds component attached to this gameobject (assumes its existance)
        _cChecks = GetComponent<CollisionChecks>();//Finds component attached to this gameobject (assumes its existance)
        _rb = GetComponent<Rigidbody2D>();//Finds component attached to this gameobject (assumes its existance)
        _col = GetComponent<Collider2D>();//Finds component attached to this gameobject (assumes its existance)
        _pShoot = GetComponent<PlayerShoot>();//Finds component attached to this gameobject (assumes its existance)
        _vCam = FindObjectOfType<CinemachineVirtualCamera>();//Finds component of type in the scene (assumes its existance and that there is only one)
        _gManager = FindObjectOfType<GameManager>();//Finds component of type in the scene (assumes its existance and that there is only one)
    }

    //Public Update is called once per frame from GameManager when in a playable state.
    //This enables the player functionality to only be in use while the game is in certain states.
    public void PlayerUpdate()
    {
        _pControl.InputUpdate();//Runs player inputs
        _cMove.MoveUpdate();//Runs player motion
        _cJump.JumpUpdate();//Runs player jump
        _pShoot.ShootUpdate();//Runs player shooting
        _cChecks.CollisionUpdate();//Runs player collisions
    }

    //Public function enables other classes to set player to dead state
    public void PlayerDeath() 
    {
        _rb.simulated = false;//Stops all physics based motion on the player character.
        _col.enabled = false;//Prevents any future collisions with the player.
        _vCam.enabled = false;//Prevents the camera from tracking the player character.
        _gManager.SetState(GameManager.GameState.playerDied);//Sets the game state to player died (This stops the above PlayerUpdate being called).
        Invoke("DeathAnim", 1f);//Invokes player death animation with delay.
    }

    //Runs player death animation
    void DeathAnim() 
    {
        _rb.simulated = true;//Turns physics back on.
        _rb.velocity = Vector2.zero;//Sets velocity to zero.
        _rb.AddForce((transform.up * 7.5f), ForceMode2D.Impulse);//Adds upwards force to player
        Invoke("ResetGame", 2f);//Invokes reseting the game with delay
    }

    //Gets the game into a state to reset.
    void ResetGame() 
    {
        _gManager.SetState(GameManager.GameState.reset);//Sets game state to reset
        _rb.velocity = Vector2.zero;//Set player velocity to 0 in all axis (technicaly probably not needed).
        _rb.simulated = false;//Stops physics simulation on the player.
    }
}
