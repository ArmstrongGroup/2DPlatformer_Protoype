using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Shooting pickup
//Turns shooting functionality in the player character on
public class ShootingPickup : MonoBehaviour
{
    //Private reference variables 
    TriggerScript _trigger;//Reference to trigger script
    PlayerShoot _pShoot;//Reference to player shoot
    SpriteRenderer _sRend;//Reference to the sprite renderer of the pickup

    // Awake is called before the first frame update and start
    void Awake()
    {
        _trigger = GetComponentInChildren<TriggerScript>();//Assigns the trigger script variable to one on the child game object (assumes there is a child object with a TriggerScript component)
        _pShoot = FindObjectOfType<PlayerShoot>();//Assigns the player shoot variable to one attached to the player character (assumes there only one in the scene)
        _sRend = GetComponent<SpriteRenderer>();//Assigns the sprite render variable to one attached to this object (assumes there is a sprite renderer on this object)
    }
    // Start is called before the first frame update
    void Start()
    {
        _trigger.myTriggerEntered += PickedUp;//Adds picked up function to delegate in the trigger script
        //This ensure picked up is called when the trigger is entered by an object with the correct tag
    }

    //Turns of pick up and turns on shooting
    void PickedUp() 
    {
        _trigger.enabled = false;//Turns trigger script off to prevent future calls
        _sRend.enabled = false;//Turns renderer off so player can no longer see pick up
        _pShoot.hasGun = true;//Enables shooting in the player
    }

    //Called when game object is disabled
    private void OnDisable()
    {
        _trigger.myTriggerEntered -= PickedUp;//Removes picked up function from delegate in the trigger script
        //This is to ensure an error does not occur should this object no longer exist or be enabled
    }
}
