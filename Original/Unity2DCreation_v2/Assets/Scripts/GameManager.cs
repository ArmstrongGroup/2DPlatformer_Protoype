using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not added (there be default) and is in use by using Lists.
using UnityEngine;//Not added (there be default) and is in use.

/*GameManager class is used as the central tick of the game
 * The update function here is used to call all other gameplay critical to gameplay
 * This so the execution order of the gameplay can be controlled 
 * and funcionality can be paused/stopped through the transitioning of states (independant or the editor built in time scale)
 */
public class GameManager : MonoBehaviour
{
    //GameState stores the current state of the game
    //From this, all other functionaliy can be run so it only takes place in chosen states
    public enum GameState {menu, inPlay, playerDied, playerDead, reset};//Defines new type of object, GameState, and its possible values
    public GameState _gState = GameState.inPlay; // Level loads with the play state running

    //Lists that store a refernce to all enemies controllers and moving platforms controller scripts in the level
    //This is so they can all be run from here and be played, paused and reset as and when using the GameState
    public List<EnemyStateController> _enemies = new List<EnemyStateController>();
    public List<MovingPlatform> _movePlatforms = new List<MovingPlatform>();

    //Refernce to the player controller so the player can be run from here
    PlayerStateController _pStateCont;
    
    //UI elements
    //These are turned on and off depending on GameState
    GameObject _deathUI;
    GameObject _playUI;
    InventoryMotion _invMotion;

    // Start is called before the first frame update
    void Start()
    {
        //Assigns variables to references in the scene
        _pStateCont = FindObjectOfType<PlayerStateController>(); //Uses type of object to assign Player State Controller (there must be only one in the scene)
        _invMotion = FindObjectOfType<InventoryMotion>(); //Uses type of object to assign Inventory Motion (there must be only one in the scene)
        _deathUI = GameObject.Find("DeathUI"); //Uses game object name to assign DeathUI object (there must be only one in the scene)
        _deathUI.SetActive(false); //Turns DeathUI object off as start state of game has the player alive
        _playUI = GameObject.Find("PlayUI");//Uses game object name to assign PlayUI object (there must be only one in the scene)

        //Cursor setting
        Cursor.visible = false; //Turns cursor off as it is not needed for gameplay
        Cursor.lockState = CursorLockMode.Locked; //Locks cursor (places it in the centre of the screen)

        //Run find functions
        FindEnemies(); //Finds all enemies and puts them in a list
        FindPlatforms(); //Finds all moving platforms and puts them in a list
    }

    //Finds all enemies in scene and adds their controller scripts to a list
    void FindEnemies() 
    {
        //Temporary variable enems, an array of game objects has all enemies in the scene added to it
        GameObject[] enems = GameObject.FindGameObjectsWithTag("Enemy");//Enemies found via their tag. This is bad! 

        //Every enemy in enems has its EnemyStateController added to list
        //(Assumes all object tagged 'Enemy' has an EnemyStateController component). This is bad!   
        foreach (GameObject obj in enems) 
        {
            _enemies.Add(obj.GetComponent<EnemyStateController>());
        }
    }

    //Finds all enemies in scene and adds their controller scripts to a list
    void FindPlatforms()
    {
        //Temporary variable plats, an array of game objects has all moving platforms in the scene added to it
        GameObject[] plats = GameObject.FindGameObjectsWithTag("MovingPlatform");//Moving Platforms found via their tag. This is bad!  

        //Every moving platform in plats has its Moving Platfom script added to list
        //(Assumes all object tagged 'MovingPlatform' has a MovingPlatform component). This is bad!    
        foreach (GameObject obj in plats)
        {
            _movePlatforms.Add(obj.GetComponentInParent<MovingPlatform>());
        }
    }

    // Update is called once per frame
    // This is the heartbeat of the game
    void Update()
    {
        //Runs game manager state machine
        StateMachine();
    }

    //Runs functionality based upon game state 
    void StateMachine() 
    {
        switch (_gState)//Assess current game state 
        {
            //Run when game state is inPlay
            case GameState.inPlay:
                RunPlatforms();//Moving Platforms are run. See below function for details.
                //Moving platforms are run first.
                //This is important as velocity from these can affect the player (as it is added to the player's velocity when in contact).
                //So this must happen prior to the player update.
                //Not doing this can lead to jitter when the player is on moving platform.

                _pStateCont.PlayerUpdate();//Player update is run. See referenced object for details
                _invMotion.InvMotionUpdate();//Inventory motion is run. See referenced object for details
                RunEnemies(); //Enemies are run. See below function for details.
                break;
            //Run when player dies.
            case GameState.playerDied:
                StopEnemies(); //Stops all enemy motion
                StopPlatforms();//Stops all moving platform motion
                _gState = GameState.playerDead; //Changes state to prevent continual call of functions
                break;
            //Holding state while player is dead
            case GameState.playerDead:

                break;
            //Enables game reset.
            case GameState.reset:
                Cursor.visible = true; //Shows cursor
                Cursor.lockState = CursorLockMode.Confined;//Unlocks cursor and allows player control via mouse
                _playUI.SetActive(false);//Hides UI from game
                _deathUI.SetActive(true);//Shows UI to allow play
                _gState = GameState.menu;//Changes state to prevent the continual calling of functionality
                break;
            //Holding state while in menu.
            case GameState.menu:

                break;
            //Catches any other game state. Should not happen but will debug an error to the console.
            default:
                Debug.LogError("Invalid Game State");
                break;
        }
        
    }

    //Runs all enemies found in the scene via FindEnemies
    void RunEnemies() 
    {
        if (_enemies != null) //Checks that there are enemies in the list
        {
            for (int i = 0; i < _enemies.Count; i++) //Loops through all enemies in the list every frame
            {
                if (_enemies[i] == null) //If an index is empty, referencing nothing, it is then removed.
                {
                    _enemies.Remove(_enemies[i]); //This is one of the reasons to use a list over an array.
                }
                else 
                {
                    _enemies[i].EnemyUpdate();//Runs the Update functionality on reference i. See the EnemyStateController class for more detail.
                }
            }
        }
    }

    //Runs all Moving Platforms found in the scene via FindPlatforms
    void RunPlatforms()
    {
        if (_movePlatforms != null)//Checks that there are moving platforms in the list
        {
            for (int i = 0; i < _movePlatforms.Count; i++) //Loops through all moving platforms in the list every frame
            {
                if (_movePlatforms[i] == null) //If an index is empty, referencing nothing, it is then removed.
                {
                    _movePlatforms.Remove(_movePlatforms[i]); //This is one of the reasons to use a list over an array.
                }
                else
                {
                    _movePlatforms[i].PlatformUpdate();//Runs the Update functionality on reference i. See the MovingPlatform class for more detail.
                }
            }
        }
    }

    //Stops all enemies found in the scene via FindEnemies
    void StopEnemies() 
    {
        if (_enemies != null)//Checks that there are enemies in the list
        {
            for (int i = 0; i < _enemies.Count; i++)//Loops through all enemies in the list every frame
            {
                if (_enemies[i] == null)//If an index is empty, referencing nothing, it is then removed.
                {
                    _enemies.Remove(_enemies[i]);//This is one of the reasons to use a list over an array.
                }
                else
                {
                    _enemies[i].Stop();//Runs the Stop functionality on reference i. See the EnemyStateController class for more detail.
                }
            }
        }
    }

    //Runs all Moving Platforms found in the scene via FindPlatforms
    void StopPlatforms()
    {
        if (_movePlatforms != null)//Checks that there are moving platforms in the list
        {
            for (int i = 0; i < _movePlatforms.Count; i++)//Loops through all moving platforms in the list every frame
            {
                if (_movePlatforms[i] == null)//If an index is empty, referencing nothing, it is then removed.
                {
                    _movePlatforms.Remove(_movePlatforms[i]);//This is one of the reasons to use a list over an array.
                }
                else
                {
                    _movePlatforms[i].Stop();//Runs the Stop functionality on reference i. See the MovingPlatform class for more detail.
                }
            }
        }
    }

    //Public function that enables to game state to be set from another class.
    public void SetState(GameState state) 
    {
        _gState = state; //Sets game state to the requested state passed to the function.
    }
}
