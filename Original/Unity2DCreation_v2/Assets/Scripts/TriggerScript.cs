using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Trigger script enables the reuse of this functionality without writting the code multiple times
//This extends the functionality through use of a delegate, meaning multiple other components can subscribe to the same OnTrigger functionality
//Could be extended further by using an array for effectedTag and looping through multiple entries. This would enable a single OnTrigger function to assess multiple tags as correct for a single piece of functionality
public class TriggerScript : MonoBehaviour
{
    //Public variables
    //inTrigger should just be gettable
    public bool inTrigger = false;//Stores state of whether a correctly tagged object is in the trigger
    //This could be a private serialized field
    public string effectedTag;//Stores the tag that triggers the functionality

    //Public delegate that is called on trigger entry
    public delegate void TriggerEntered();//Delegate is defined
    public TriggerEntered myTriggerEntered;//Instance of delegate created

    //Public delegate that is called on trigger exit
    public delegate void TriggerExited();//Delegate is defined
    public TriggerExited myTriggerExited;//Instance of delegate created

    //UnityEngine function called when a 2D collider attached to this object is marked as a Trigger and entered by another object with a 2D collider
    //One of the objects, at least, must also have a rigidbody2D component
    private void OnTriggerEnter2D(Collider2D collider)//collider stores the collider2D of the object entering the trigger
    {
        if (collider.tag == effectedTag)//Tests if entering object is tagged with the effectedTag 
        {
            inTrigger = true;//Sets inTrigger state to true (this would need to change if multiple objects with effectedTag can enter and exit the trigger)

            if (myTriggerEntered != null)//Tests if there are any subscribing functions to the delegate for entering 
            {
                myTriggerEntered();//Calls all subscibed functionality
            }
        }
    }

    //UnityEngine function called when a 2D collider attached to this object is marked as a Trigger and is exited by another object with a 2D collider
    //One of the objects, at least, must also have a rigidbody2D component
    private void OnTriggerExit2D(Collider2D collider)//collider stores the collider2D of the object entering the trigger
    {
        if (collider.tag == effectedTag)//Tests if exiting object is tagged with the effectedTag 
        {
            inTrigger = false;//Sets inTrigger state to false (this would need to change if multiple objects with effectedTag can enter and exit the trigger)

            if (myTriggerExited != null)//Tests if there are any subscribing functions to the delegate for exiting
            {
                myTriggerExited();//Calls all subscibed functionality
            }
        }
    }
}
