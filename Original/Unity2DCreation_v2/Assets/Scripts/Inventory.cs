using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not added (there be default) and is in use (Lists).
using UnityEngine.UI;//Added and in use. Needed for access to Unity UI elements
using UnityEngine;//Not added (there be default) and is in use.

//Inventory stores game objects in a list so they can be used
//A rudimentary implementation currently 
//A good candidate for using an interface to reconise and enforce inventory expectations
//A good candidate to make more robust.
//Very much a prototype version at present
public class Inventory : MonoBehaviour
{
    //Should be private, but public made debugging easier
    public List<Image> imageSlots;//List of images
    public List<GameObject> inventorySlots;//List of game objects
    //Good candidate for replacing with one list of a structs holding multiple data types within it

    //Public function used to add a game object to the inventory
    //Returns a value of true if object is successfully added the inventory
    //Else returns false
    //This is so pick up objects can assess whether they need to change state (i.e. disable themselves)
    public bool AddToInv(GameObject obj, Sprite iconSprite, Color iconColour)//Requires a game object, a sprite and a colour to work
    {//May need versions with different signitures to help extend functionality
        for (int i = 0; i < inventorySlots.Count; i++)//Loops through all inventory slots 
        {
            if (inventorySlots[i] == null)//Checks for space in the inventory (is there an empty slot) 
            {
                //Populates empty slot
                inventorySlots[i] = obj;//Stores game object in list
                imageSlots[i].sprite = iconSprite;//Stores image in parallel list
                imageSlots[i].color = iconColour;//Sets colour of icon
                return true;//Ends function by returning true (object added to inventory)
            }
        }
        return false;//Ends function by returning false (object not added to inventory)
    }

    //Removes object from inventory
    public void RemoveFromInv(GameObject obj) 
    {
        for (int i = 0; i < inventorySlots.Count; i++)//Loops through all inventory slots
        {
            if (inventorySlots[i] == obj)//Checks if index equals object to remove
            {
                //Removes object
                inventorySlots[i] = null;//Sets slot to empty
                imageSlots[i].sprite = null;//Sets sprite to empty
                imageSlots[i].color = new Color(1,1,1,0);//Sets sprite colour to transparent 

                InventoryReset();//Resets inventory to reorder objects left
            }
        }
    }

    //Reorders objects in inventory
    void InventoryReset() 
    {
        for (int i = 0; i < inventorySlots.Count; i++)//Loops through all slots
        {
            if (inventorySlots[i] == null)
            {
                if (i != inventorySlots.Count - 1)//Checks that we are not going to assess the last index as this will error 
                {
                    inventorySlots[i] = inventorySlots[i + 1];//Sets the current game object index to the next index
                    imageSlots[i].sprite = imageSlots[i + 1].sprite;//Sets the current sprite index to the next index
                    imageSlots[i].color = imageSlots[i + 1].color;//Sets the current sprite colour index to the next index

                    //Empties the next index to avoid duplication
                    inventorySlots[i + 1] = null;//Empties game object so equals null
                    imageSlots[i + 1].sprite = null;//Empties sprite so equals null
                    imageSlots[i + 1].color = new Color(1, 1, 1, 0);//Sets sprite colour to transparent 
                }
            }
        }
    }
}
