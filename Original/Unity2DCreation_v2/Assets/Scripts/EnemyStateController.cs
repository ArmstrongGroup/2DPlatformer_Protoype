using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Enemy State Controller moves the state of the enemy from active to dead
public class EnemyStateController : MonoBehaviour
{
    //Private variables
    enum EnemyState { active, dead};//Defines enemy state type
    EnemyState _eState = EnemyState.active;//Create instance of type EnemyState

    //Reference variables
    EnemyMovement _eMove;//Reference to enemy movement
    EnemyCollisionChecks _cChecks;//Reference to collision checks
    Rigidbody2D _rb;//Reference to rigidbody
    Collider2D _col;//Reference to collider

    bool falling;//Stores state of enemy falling

    // Start is called before the first frame update
    void Start()
    {
        //Assign reference variables 
        _eMove = GetComponent<EnemyMovement>();//Assigns EnemyMovement to the variable (assumes there is a component of EnemyMovement attached to this object)
        _cChecks = GetComponent<EnemyCollisionChecks>();//Assigns EnemyCollisionChecks to the variable (assumes there is a component of EnemyCollisionChecks attached to this object)
        _rb = GetComponent<Rigidbody2D>();//Assigns Rigidbody2D to the variable (assumes there is a component of Rigidbody2D attached to this object)
        _col = GetComponent<Collider2D>();//Assigns Collider2D to the variable (assumes there is a component of Collider2D attached to this object)
    }

    // Update is called once per frame from the GameManager class
    public void EnemyUpdate()
    {
        //Calls below StateMachine function
        StateMachine();
    }

    void StateMachine() 
    {
        switch (_eState)//Tests to see what state the enemy is in 
        {
            case EnemyState.active://Runs when enemy is active
                _cChecks.CollisionUpdate();//Calls collision functionality
                _eMove.MoveUpdate();//Calls movement functionality
                FallCheck();//Runs fall check
                break;
            case EnemyState.dead://Runs when enemy is dead
                //Does nothing
                break;
            default:
                //In theory never called. Could put error message here
                break;
        }
    }

    //Public Die function called when enemy is killed
    //At present this is called via Broadcast
    //This creates an issue with the function appearing to have no references 
    public void Die() 
    {
        _eState = EnemyState.dead;//Sets state to dead
        _col.enabled = false;//Turns off collider
        _rb.velocity = Vector2.zero;//Stops enemy moving
        _rb.AddForce(transform.up * 5, ForceMode2D.Impulse);//Adds upwards force to the enemy (death animation)
        Invoke("Death", 2);//Invokes function to destroy the enemy object after 2 seconds
    }

    //Checks if enemy has fallen of a ledge and should be destroyed
    //This feels like it is breaking the single responsability principle
    //Though this could be argued will all the other death functionality in this class
    void FallCheck() 
    {
        //Checks if the enemy is not on the ground and falling state is false
        if (!_cChecks.OnGround && !falling)//If object is falling 
        {
            falling = true;//Set falling to true
            Invoke("Death", 2);//Invoke death
        }
        if (_cChecks.OnGround && falling)//If object is marked as falling but is on the ground 
        {
            falling = false;//Sets falling to false
            CancelInvoke("Death");//Cancels death
        }
    }

    //Death function
    //Invoked above, destroys this instance of the enemy object
    void Death() 
    {
        Destroy(gameObject);//Destroys object
    }

    //Stop ceases all motion
    public void Stop() 
    {
        _rb.velocity = Vector2.zero;//Sets velocity to zero
        _rb.simulated = false;//Stops any physics simulation taking place
        CancelInvoke();//Cancels invokes to prevent prior death calls making the object disapear
    }
}
