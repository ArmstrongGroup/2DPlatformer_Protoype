using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Enemy collision checks detects all overlapping objects and decides what action should be taken
//Class is very similar to the player's collision checks script. 
//This makes them possibly good canditates for use of inheritance. Especially as the outputs are basicly the same
public class EnemyCollisionChecks : MonoBehaviour
{
    //Public properties
    public bool OnGround { get { return _onGround; } }
    bool _onGround;//Stores state of grounding
    public bool LeftBlock { get { return _leftBlock; } }
    bool _leftBlock;//Stores state of obstacles left
    public bool RightBlock { get { return _rightBlock; } }
    bool _rightBlock;//Stores state of obstacles left

    //These could be private and serialised
    public LayerMask collisionsLayerMaskGround;//Stores what layers will be detected
    public Vector2 collisionsSize = new Vector2(0.5f, 1.0f);//Stores size of object

    float midPoint;//Stores midpoint offset from pivot in y of collider
    Vector2 tenPercent;//Stores the variabe of 10% the size of the collider
    RaycastHit2D[] hitters;//Stores all hits of objects intersecting with this one

    //Start function called prior to update
    private void Start()
    {
        midPoint = collisionsSize.y / 2;//Assigns midpoint y value to add to y position so BoxCast aligns correctly
        tenPercent = new Vector2(collisionsSize.x / 10, collisionsSize.y / 10);//Calculates and stores 10% of collision size
        collisionsSize = tenPercent + collisionsSize;//Adds 10% extra to the collision size. This is done to align the collisions with the Collisions detection offset
    }

    //CollisionUpdate is called once per frame from the Enemy State Controller
    //This ensures that as the game changes state the 
    public void CollisionUpdate()
    {
        Collisions();//Calls collision functionality
    }

    //Uses Box Cast to detect all collisions every frame
    void Collisions()
    {
        //Stores all hits in the array hitters
        hitters = Physics2D.BoxCastAll(new Vector2(transform.position.x, transform.position.y + midPoint), collisionsSize, 0f, Vector2.zero, 0f, collisionsLayerMaskGround, -0.1f, 0.1f);

        //Sets collision states to false
        _onGround = false;
        _leftBlock = false;
        _rightBlock = false;

        //Tests if there are any collisions that need assessing
        if (hitters.Length > 0)
        {
            //Loops through each collision hit
            for (int i = 0; i < hitters.Length; i++)
            {
                //Stops enemies detecting themselves
                if (hitters[i].collider.gameObject != gameObject)
                {
                    //Only runs if prior index in array has not set onGround
                    if (!_onGround)
                    {
                        //Tests if hit position is below enemy's position
                        if (hitters[i].point.y < transform.position.y)
                        {
                            _onGround = true; //Sets onGround to true
                        }
                    }

                    //Only runs if prior index in array has not set right block
                    if (!_rightBlock)
                    {
                        //Checks if the hit is on the left 
                        if (hitters[i].point.x < transform.position.x)
                        {
                            //Does nothing, could nest this differently
                        }
                        //Knowing the hit is on the right, checks the hit is not on the top of the collider
                        else if (hitters[i].point.y >= transform.position.y && hitters[i].point.y < (transform.position.y + (midPoint * 2)))
                        {
                            _rightBlock = true;//Sets right block to true
                        }
                    }
                    //Only runs if prior index in array has not set light block
                    if (!_leftBlock)
                    {
                        //Checks if the hit is on the right 
                        if (hitters[i].point.x > transform.position.x)
                        {
                            //Does nothing, could nest this differently
                        }
                        //Knowing the hit is on the left, checks the hit is not on the top of the collider
                        else if (hitters[i].point.y >= transform.position.y && hitters[i].point.y < (transform.position.y + (midPoint * 2)))
                        {
                            _leftBlock = true;//Sets right block to true
                        }
                    }
                }
            }
        }
    }
}
