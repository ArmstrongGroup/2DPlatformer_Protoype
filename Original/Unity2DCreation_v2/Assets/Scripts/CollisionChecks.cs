using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Collision checks detects all overlapping objects and decides what action should be taken
//Class is very similar to the enemy's collision checks script. 
//This makes them possibly good canditates for use of inheritance. Especially as the outputs are basicly the same
//This class should also probably be split into 2. It actually does 2 things, assess collisions and apply velocity from moving platforms
//Should really just do one of these.
public class CollisionChecks : MonoBehaviour
{
    //Gives range of amount of velocity to add from platform when leaving it
    //1 = 100%
    //0 = none
    [Range(0.0f, 1.0f)]
    public float amountVelAdded = 0.75f;//This could be a private serialized field

    //Public variables
    //These should be converted to gettable only
    public bool OnGround
    {get { return _onGround; }}
    bool _onGround;//Stores state of grounding

    public bool OnPlat { get { return _onPlat; } }
    bool _onPlat;//Stores state of platform

    //These could be private and serialised
    public LayerMask collisionsLayerMask;//Stores what layers will be detected
    public Vector2 collisionsSize = new Vector2(0.5f, 1.0f);//Stores size of object

    RaycastHit2D[] hitters;//Stores all hits of objects intersecting with this one
    float midPoint;//Stores midpoint offset from pivot in y of collider
    Vector2 tenPercent;//Stores the variabe of 10% the size of the collider
    float _initialGrav; //Store start value of gravity
    

    //Reference variables
    PlayerStateController _pStateCont;//Stores refeernce to the player state contoller
    Rigidbody2D _rb;//Stores reference to the rigidbody 
    CharacterJump _cJump;//Stores reference to the player jumping script
    Collider2D _col;//Staores reference to player collider

    Rigidbody2D _platRB;//Stores a reference to the moving platform's rigidbody
    Collider2D _platCOL;//Stores a reference to the moving platform's collider
    Vector2 _addVelocity;//Stores the amount of velocity to add to the player velocity based upon movining platforms

    //Storess the state of the player character in relation to a platform
    enum PlatformState {noPlatform, onPlatform, leftPlatform, jumpOff};//Defines state type
    PlatformState _platState = PlatformState.noPlatform;//Creates instance of state for use here

    //Start function called prior to update
    void Start()
    {
        midPoint = collisionsSize.y / 2;//Assigns midpoint y value to add to y position so BoxCast aligns correctly
        tenPercent = new Vector2(collisionsSize.x / 10, collisionsSize.y / 10);//Calculates and stores 10% of collision size
        collisionsSize = tenPercent + collisionsSize;//Adds 10% extra to the collision size. This is done to align the collisions with the Collisions detection offset
        _col = GetComponent<Collider2D>();
        _pStateCont = GetComponent<PlayerStateController>();//Populates player state controller into a variable
        _rb = GetComponent<Rigidbody2D>();//Populates rigidbody2D into a variable
        _cJump = GetComponent<CharacterJump>();//Populates character jump into a variable
        _platState = PlatformState.noPlatform;//Sets platform state to no platform
        _platRB = null;//Sets platform rigidbody2D variable to nothing
        _cJump.myJump += ReleasePlayer;//Adds release player function to delegate in character jump assigned above
        _initialGrav = _rb.gravityScale;
    }

    // Update is called once per frame
    public void CollisionUpdate()
    {
        Collisions();//Runs collisions
        VelocityUpdate();//Runs velocity update
    }

    void Collisions()
    {
        //Stores all hits in the array hitters
        hitters = Physics2D.BoxCastAll(new Vector2(transform.position.x, transform.position.y + midPoint), collisionsSize, 0f, Vector2.zero, 0f, collisionsLayerMask, -0.1f, 0.1f);

        //Sets collision states to false
        _onGround = false;
        //_leftBlock = false;
        //_rightBlock = false;

        if (hitters.Length > 0)//Tests if there are any collisions that need assessing
        {
            //Loops through each collision hit
            for (int i = 0; i < hitters.Length; i++)
            {
                //Only runs if prior index in array has not set onGround
                if (!_onGround)
                {
                    //Tests if hit position is above player position and checks that the hitting object is not an enemy
                    if (hitters[i].point.y > transform.position.y && !hitters[i].collider.CompareTag("Enemy"))
                    {
                        _onGround = false;//Sets grounding to false
                    }
                    else
                    {
                        _onGround = true;//Sets grounding to true
                    }
                }
                //Checks if player has hit a hazard
                if (hitters[i].collider.gameObject.CompareTag("Hazard"))
                {
                    KillPlayer();//Runs kill player if hazard is hit
                }
                //If not a hazard, checks if the player has hit an enemy
                else if (hitters[i].collider.CompareTag("Enemy"))
                {
                    //Check whether the player has landed on the enemies head
                    if (hitters[i].collider.gameObject.transform.position.y > (transform.position.y - 0.4f)) //0.4 variable should be elevated to a variable that can be tweaks
                    {
                        KillPlayer();//Runs kill player
                    }
                    else //Kills enemy collided with
                    {
                        hitters[i].collider.gameObject.BroadcastMessage("Die");//Sends kill message
                        _rb.velocity = Vector2.zero;//Zeros motion
                        _rb.AddForce(transform.up * 7.5f, ForceMode2D.Impulse);//Applies small jump
                    }
                }
            }
        }
        else if (NoPlatform())//Checks for platform
        {
            _platState = PlatformState.leftPlatform; //Sets state to leave platform
            _rb.gravityScale = _initialGrav;//Sets gravity back to normal value
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) //Runs on collisions
    {
        if (collision.gameObject.tag == "MovingPlatform") //Checks colliding object is a moving platform
        {
            _platCOL = collision.gameObject.GetComponent<Collider2D>(); //Stores collider of platform

            if (_platCOL.transform.position.y < transform.position.y)//Checks platform is under player
            {
                _platState = PlatformState.onPlatform;//Sets player to on platform
                _rb.gravityScale = 0; // Sets gravity to 0
                _platRB = collision.gameObject.GetComponent<Rigidbody2D>();//Gets rb of platform
                _onPlat = true;//Stores state of being on the platform
            }
        }
        else 
        {
            _platState = PlatformState.noPlatform; //Sets state to no platform
            _addVelocity = Vector2.zero; //Zeros add velocity
            _onPlat = false;//Stores state of being on the platform
        }
    }

    //Function tests if platform is under player
    bool NoPlatform() 
    {
        if (_platCOL != null)//Checks player has collided with a platform
        {
            if ((_platCOL.bounds.extents.x + _col.bounds.extents.x) + _platCOL.transform.position.x < transform.position.x)//Checks left edge
            {
                _platCOL = null;//Empties platform collider
                return true;//Returns result ending function
            }
            else if ((_platCOL.transform.position.x - _platCOL.bounds.extents.x) - _col.bounds.extents.x > transform.position.x)//Checks right edge
            {
                _platCOL = null;//Empties platform collider
                return true;//Returns result ending function
            }
        }
        return false;//Returns result ending function
    }

    //Updates the velocity of the player by adding moving platform velocity
    private void VelocityUpdate()
    {
        switch (_platState)//Checks platform state
        {
            case PlatformState.leftPlatform://If player has left platform
                _rb.velocity = _rb.velocity + _addVelocity;//Adds velocity from platform
                break;
            case PlatformState.onPlatform://If player is on platform
                _onGround = true;//Sets on ground to true
                _addVelocity.x = _platRB.velocity.x;//Sets add velocity
                _rb.velocity = new Vector2(_rb.velocity.x + _platRB.velocity.x, _platRB.velocity.y);//Sets player velocity to input plus platform
                break;
            default:
                break;
        }
    }

    //Release player function allows the player character to escape the platform
    void ReleasePlayer()
    {
        if (_platState == PlatformState.onPlatform)//Tests if player is on a platform
        {
            _platState = PlatformState.leftPlatform;//Sets state to jump off
            _addVelocity.x = _addVelocity.x * amountVelAdded;//Sets add velocity to percentage 
            _rb.gravityScale = _initialGrav;//Sets gravity scale back to 2
            _onGround = false;//Sets on ground to false
            _onPlat = false;//Sets on platform to false
        }
    }

    //Runs the player death functionality stored else where
    void KillPlayer()
    {
        _pStateCont.PlayerDeath();//Runs player death on player state controller
    }
}
