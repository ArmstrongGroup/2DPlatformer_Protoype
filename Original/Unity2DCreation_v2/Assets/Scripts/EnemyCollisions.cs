using System.Collections;// Not needed(there be default) and could be removed.
using System.Collections.Generic;// Not needed(there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.


//Enemy collisions uses the physics system to detect if the enemy has been hit by a bullet
//Possibly should be moved into the collision checks system, though this works fine for now
//Class name is also misleading. Only deals with projectile collisons
public class EnemyCollisions : MonoBehaviour
{
    //Is called upon a collision event via the physics system in Unity
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Tests if the colliding object is tagged PlayerProjectile
        //Interfaces may be better than tags for this, especially if functionality is moved into the box cast system in Enemy Collision Checks
        if (collision.collider.CompareTag("PlayerProjectile")) 
        {
            BroadcastMessage("Die");//Runs die functionality on enemy
            //This could be seen as a little lazy, could get refernce to EnemyStateController
        }
    }
}
