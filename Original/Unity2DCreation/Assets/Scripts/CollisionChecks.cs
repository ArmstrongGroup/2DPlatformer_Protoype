using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Collision checks detects all overlapping objects and decides what action should be taken
//Class is very similar to the enemy's collision checks script. 
//This makes them possibly good canditates for use of inheritance. Especially as the outputs are basicly the same
//This class should also probably be split into 2. It actually does 2 things, assess collisions and apply velocity from moving platforms
//Should really just do one of these.
public class CollisionChecks : MonoBehaviour
{
    //Public variables
    //These should be converted to gettable only
    public bool onGround;//Stores state of grounding
    public bool leftBlock;//Stores state of obstacles left
    public bool rightBlock;//Stores state of obstacles left

    //These could be private and serialised
    public LayerMask collisionsLayerMask;//Stores what layers will be detected
    public Vector2 collisionsSize = new Vector2(0.5f, 1.0f);//Stores size of object

    RaycastHit2D[] hitters;//Stores all hits of objects intersecting with this one
    float midPoint;//Stores midpoint offset from pivot in y of collider
    Vector2 tenPercent;//Stores the variabe of 10% the size of the collider

    //Gives range of amount of velocity to add from platform when leaving it
    //1 = 100%
    //0 = none
    [Range(0.0f, 1.0f)]
    public float amountVelAdded = 0.75f;//This could be a private serialized field

    //Reference variables
    PlayerStateController _pStateCont;//Stores refeernce to the player state contoller
    Rigidbody2D _rb;//Stores reference to the rigidbody 
    CharacterJump _cJump;//Stores reference to the player jumping script

    Rigidbody2D _platRB;//Stores a reference to the moving platform's rigidbody
    Vector2 _addVelocity;//Stores the amount of velocity to add to the player velocity based upon movining platforms
    bool _preventYVelocityApplication;//Prevents continued application to velocity in y, limiting it to an impulse

    //Storess the state of the player character in relation to a platform
    enum PlatformState {noPlatform, onPlatform, leftPlatform, jumpOff};//Defines state type
    PlatformState _platState = PlatformState.noPlatform;//Creates instance of state for use here

    //Start function called prior to update
    void Start()
    {
        midPoint = collisionsSize.y / 2;//Assigns midpoint y value to add to y position so BoxCast aligns correctly
        tenPercent = new Vector2(collisionsSize.x / 10, collisionsSize.y / 10);//Calculates and stores 10% of collision size
        collisionsSize = tenPercent + collisionsSize;//Adds 10% extra to the collision size. This is done to align the collisions with the Collisions detection offset

        _pStateCont = GetComponent<PlayerStateController>();//Populates player state controller into a variable
        _rb = GetComponent<Rigidbody2D>();//Populates rigidbody2D into a variable
        _cJump = GetComponent<CharacterJump>();//Populates character jump into a variable
        _platState = PlatformState.noPlatform;//Sets platform state to no platform
        _platRB = null;//Sets platform rigidbody2D variable to nothing
        _cJump.myJump += ReleasePlayer;//Adds release player function to delegate in character jump assigned above
    }

    // Update is called once per frame
    public void CollisionUpdate()
    {
        Collisions();//Runs collisions
        VelocityUpdate();//Runs velocity update
    }

    /*/Uses Box Cast to detect all collisions every frame
    void Collisions()
    {
        //Stores all hits in the array hitters
        hitters = Physics2D.BoxCastAll(new Vector2(transform.position.x, transform.position.y + midPoint), collisionsSize, 0f, Vector2.zero, 0f, collisionsLayerMask, -0.1f, 0.1f);
        
        //Sets collision states to false
        onGround = false;
        leftBlock = false;
        rightBlock = false;

        if (hitters.Length > 0)//Tests if there are any collisions that need assessing
        {
            //Loops through each collision hit
            for (int i = 0; i < hitters.Length; i++)
            {
                //if (hitters[i].collider.gameObject.layer == 0)
                //Only runs if prior index in array has not set onGround
                if (!onGround)
                {
                    //Tests if hit position is above player position and checks that the hitting object is not an enemy
                    if (hitters[i].point.y > transform.position.y && !hitters[i].collider.CompareTag("Enemy"))
                    {
                        onGround = false;//Sets grounding to false
                    }
                    else
                    {
                        onGround = true;//Sets grounding to true
                    }  
                }
                //Only runs if prior index in array has not set right block
                if (!rightBlock)
                {
                    //Checks his is not on the left and whenther it is an enemy
                    if (hitters[i].point.x < transform.position.x && !hitters[i].collider.CompareTag("Enemy"))
                    {
                        rightBlock = false;//Sets block to false
                    }
                    //Checks the hit is on the right and not on the top of the collider
                    else if (hitters[i].point.y >= transform.position.y && hitters[i].point.y < (transform.position.y + (midPoint * 2)))
                    {
                        rightBlock = true;//Sets the block to true
                    }
                }
                //Only runs if prior index in array has not set light block
                if (!leftBlock)
                {
                    //Checks his is not on the right and whenther it is an enemy
                    if (hitters[i].point.x > transform.position.x && !hitters[i].collider.CompareTag("Enemy"))
                    {
                        leftBlock = false;//Sets block to false
                    }
                    //Checks the hit is on the left and not on the top of the collider
                    else if (hitters[i].point.y >= transform.position.y && hitters[i].point.y < (transform.position.y + (midPoint*2)))
                    {
                        leftBlock = true;//Sets the block to true
                    }
                }
                //Checks if player has hit a hazard
                if (hitters[i].collider.gameObject.CompareTag("Hazard"))
                {
                    KillPlayer();//Runs kill player if hazard is hit
                }
                //If not a hazard, checks if the player has hit an enemy
                else if (hitters[i].collider.CompareTag("Enemy"))
                {
                    //Check whether the player has landed on the enemies head
                    if (hitters[i].collider.gameObject.transform.position.y > (transform.position.y - 0.4f))//0.4 variable should be elevated to a variable that can be tweaks
                    {
                        KillPlayer();//Runs kill player
                    }
                    else //Kills enemy collided with
                    {
                        hitters[i].collider.gameObject.BroadcastMessage("Die"); //Sends kill message
                        _rb.velocity = Vector2.zero;//Zeros motion
                        _rb.AddForce(transform.up * 7.5f, ForceMode2D.Impulse);//Applies small jump
                    }
                }
                //If not a hazard or enemy, checks if hitting object is a moving platform
                else if (hitters[i].collider.CompareTag("MovingPlatform"))
                {
                    //Checks that platform is below player and that the player is either falling or stationary in y (not jumping)
                    if (hitters[i].collider.transform.position.y < transform.position.y && _rb.velocity.y <= 0)
                    {
                        //Check if platform rigidbody variable is empty
                        if(_platRB == null)
                        _platRB = hitters[i].collider.GetComponent<Rigidbody2D>();//if empty assigns variable
                        
                        if(_platState != PlatformState.jumpOff || _platState != PlatformState.onPlatform)//Checks platform state and if no onPlatform but not left platform
                        _platState = PlatformState.onPlatform;//Sets state to onPlatform
                    }
                }
            }
            if (NoPlatform())//Tests if there are not any collisions tagged as a platform, returns true when there are no platforms
            {
                if (_platState == PlatformState.onPlatform)//Check if current state is onPlatform
                {
                    _platState = PlatformState.leftPlatform;//Stets state to left platform
                }
                else //if state is not onPlatform. Sets state to no platform
                {
                    _platState = PlatformState.noPlatform;//Sets state
                    _platRB = null;//Empties variable storing platform rigidbody
                }
            }
        }
        else //If no collisions are detected i.e. the player is in the air
        {
            if (_platState == PlatformState.onPlatform)//Tests if player is onPlatform
            {
                _platState = PlatformState.leftPlatform;//Sets to leaving the platform
            }
            else if(_platState == PlatformState.noPlatform && _platRB != null)//Tests if player is not on platform but rigidbody is assigned
            {
                _platRB = null;//Clears rigidbody variable
            }
        }
    }*/

    void Collisions()
    {
        //Stores all hits in the array hitters
        hitters = Physics2D.BoxCastAll(new Vector2(transform.position.x, transform.position.y + midPoint), collisionsSize, 0f, Vector2.zero, 0f, collisionsLayerMask, -0.1f, 0.1f);

        //Sets collision states to false
        onGround = false;
        leftBlock = false;
        rightBlock = false;

        if (hitters.Length > 0)//Tests if there are any collisions that need assessing
        {
            //Loops through each collision hit
            for (int i = 0; i < hitters.Length; i++)
            {
                //if (hitters[i].collider.gameObject.layer == 0)
                //Only runs if prior index in array has not set onGround
                if (!onGround)
                {
                    //Tests if hit position is above player position and checks that the hitting object is not an enemy
                    if (hitters[i].point.y > transform.position.y && !hitters[i].collider.CompareTag("Enemy"))
                    {
                        onGround = false;//Sets grounding to false
                    }
                    else
                    {
                        onGround = true;//Sets grounding to true
                    }
                }
                //Only runs if prior index in array has not set right block
                if (!rightBlock)
                {
                    //Checks his is not on the left and whenther it is an enemy
                    if (hitters[i].point.x < transform.position.x && !hitters[i].collider.CompareTag("Enemy"))
                    {
                        rightBlock = false;//Sets block to false
                    }
                    //Checks the hit is on the right and not on the top of the collider
                    else if (hitters[i].point.y >= transform.position.y && hitters[i].point.y < (transform.position.y + (midPoint * 2)))
                    {
                        rightBlock = true;//Sets the block to true
                    }
                }
                //Only runs if prior index in array has not set light block
                if (!leftBlock)
                {
                    //Checks his is not on the right and whenther it is an enemy
                    if (hitters[i].point.x > transform.position.x && !hitters[i].collider.CompareTag("Enemy"))
                    {
                        leftBlock = false;//Sets block to false
                    }
                    //Checks the hit is on the left and not on the top of the collider
                    else if (hitters[i].point.y >= transform.position.y && hitters[i].point.y < (transform.position.y + (midPoint * 2)))
                    {
                        leftBlock = true;//Sets the block to true
                    }
                }
                //Checks if player has hit a hazard
                if (hitters[i].collider.gameObject.CompareTag("Hazard"))
                {
                    KillPlayer();//Runs kill player if hazard is hit
                }
                //If not a hazard, checks if the player has hit an enemy
                else if (hitters[i].collider.CompareTag("Enemy"))
                {
                    //Check whether the player has landed on the enemies head
                    if (hitters[i].collider.gameObject.transform.position.y > (transform.position.y - 0.4f))//0.4 variable should be elevated to a variable that can be tweaks
                    {
                        KillPlayer();//Runs kill player
                    }
                    else //Kills enemy collided with
                    {
                        hitters[i].collider.gameObject.BroadcastMessage("Die"); //Sends kill message
                        _rb.velocity = Vector2.zero;//Zeros motion
                        _rb.AddForce(transform.up * 7.5f, ForceMode2D.Impulse);//Applies small jump
                    }
                }
                //If not a hazard or enemy, checks if hitting object is a moving platform
                else if (hitters[i].collider.CompareTag("MovingPlatform"))
                {
                    //Checks that platform is below player and that the player is either falling or stationary in y (not jumping)
                    if (hitters[i].collider.transform.position.y < transform.position.y && _rb.velocity.y <= 0)
                    {
                        //Check if platform rigidbody variable is empty
                        if (_platRB == null) 
                        { 
                            _platRB = hitters[i].collider.GetComponent<Rigidbody2D>();//if empty assigns variable
                            _rb.gravityScale = 0;
                        }

                        if (_platState != PlatformState.jumpOff || _platState != PlatformState.onPlatform)//Checks platform state and if no onPlatform but not left platform
                            _platState = PlatformState.onPlatform;//Sets state to onPlatform
                    }
                }
            }
            if (NoPlatform())//Tests if there are not any collisions tagged as a platform, returns true when there are no platforms
            {
                if (_platState == PlatformState.onPlatform)//Check if current state is onPlatform
                {
                    _platState = PlatformState.leftPlatform;//Stets state to left platform
                }
                else //if state is not onPlatform. Sets state to no platform
                {
                    _platState = PlatformState.noPlatform;//Sets state
                    _platRB = null;//Empties variable storing platform rigidbody
                }
            }
        }
        else //If no collisions are detected i.e. the player is in the air
        {
            if (_platState == PlatformState.onPlatform)//Tests if player is onPlatform
            {
                _rb.gravityScale = 2;
                _platState = PlatformState.leftPlatform;//Sets to leaving the platform
            }
            else if (_platState == PlatformState.noPlatform && _platRB != null)//Tests if player is not on platform but rigidbody is assigned
            {
                _rb.gravityScale = 2;
                _platRB = null;//Clears rigidbody variable
            }
        }
    }

    //Function tests if any collider is tagged as a moving platform
    bool NoPlatform() 
    {
        foreach (RaycastHit2D hit in hitters)//Loops through all hits
        {
            if (hit.collider.CompareTag("MovingPlatform"))//Tests if hit object is tagged moving platform
            {
                return false;//Returns result ending function
            }
        }
        _rb.gravityScale = 2;
        return true;//Returns result ending function
    }

    /*/Updates the velocity of the player by adding moving platform velocity
    private void VelocityUpdate()
    {
        //Checks if there is no platform
        if (_platState == PlatformState.noPlatform)
        {
            _addVelocity = Vector2.zero;//Sets velocity to add to player at 0
        }
        else //Must be on a moving platform
        {
            //Check to see if we have just left the platform but have a platform rigidbody assigned
            if ((_platState == PlatformState.leftPlatform  || _platState == PlatformState.jumpOff) && _platRB != null)
            {
                //Check if y velocity is being added
                if (_preventYVelocityApplication == false)
                {
                    //Sets player velocity in y to add a percentage of the platforms velocity
                    _rb.velocity = new Vector2(_rb.velocity.x, _rb.velocity.y + (_platRB.velocity.y * (amountVelAdded)));
                    _preventYVelocityApplication = true;//Stops y velocity being added ensuring the above line only happens on one frame
                }
                _addVelocity.x = _platRB.velocity.x * amountVelAdded;// Sets the x velocity to add
                //X and Y velocities need to be treated differently as the X is effectively kinematic and the Y is physics based
                _platRB = null;//Removes rigidbody
            }
            else if (_platState == PlatformState.onPlatform)//if on platform
            {
                _addVelocity.x = _platRB.velocity.x;//Set add velocity in x to same as the platform
                _rb.velocity = new Vector2(_rb.velocity.x, _platRB.velocity.y);//Set the y velocity of the player to the same as the platform
            }
            
            if (!onGround && _platRB != null)//Checks if in the air and if there is still a platform assigned
            {
                _platState = PlatformState.leftPlatform;//Sets state to left platform
                _preventYVelocityApplication = true;//Stops y velocity application from moving platform
            }
        }
        _rb.velocity = _rb.velocity + _addVelocity;//Sets the final velocity of player
    }*/

    //Updates the velocity of the player by adding moving platform velocity
    private void VelocityUpdate()
    {
        //Checks if there is no platform
        if (_platState == PlatformState.noPlatform)
        {
            _addVelocity = Vector2.zero;//Sets velocity to add to player at 0
        }
        else //Must be on a moving platform
        {
            //Check to see if we have just left the platform but have a platform rigidbody assigned
            if ((_platState == PlatformState.leftPlatform || _platState == PlatformState.jumpOff) && _platRB != null)
            {
                //Check if y velocity is being added
                if (_preventYVelocityApplication == false)
                {
                    //Sets player velocity in y to add a percentage of the platforms velocity
                    _rb.velocity = new Vector2(_rb.velocity.x, _rb.velocity.y + (_platRB.velocity.y * (amountVelAdded)));
                    _preventYVelocityApplication = true;//Stops y velocity being added ensuring the above line only happens on one frame
                }
                _addVelocity.x = _platRB.velocity.x * amountVelAdded;// Sets the x velocity to add
                //X and Y velocities need to be treated differently as the X is effectively kinematic and the Y is physics based
                _platRB = null;//Removes rigidbody
            }
            else if (_platState == PlatformState.onPlatform)//if on platform
            {
                _addVelocity.x = _platRB.velocity.x;//Set add velocity in x to same as the platform
                _rb.velocity = new Vector2(_rb.velocity.x, _platRB.velocity.y);//Set the y velocity of the player to the same as the platform
                //transform.position = new Vector3(transform.position.x, _platRB.transform.position.y, transform.position.z);
            }

            if (!onGround && _platRB != null)//Checks if in the air and if there is still a platform assigned
            {
                Debug.Log("Poopy");
                _rb.gravityScale = 2;
                _platState = PlatformState.leftPlatform;//Sets state to left platform
                _preventYVelocityApplication = true;//Stops y velocity application from moving platform
            }
        }
        _rb.velocity = _rb.velocity + _addVelocity;//Sets the final velocity of player
    }

    //Release player function allows the player character to escape the platform
    void ReleasePlayer()
    {
        _preventYVelocityApplication = false;//Allows y velocity application to player
        if (_platState == PlatformState.onPlatform)//Tests if player is on a platform
        {
            _platState = PlatformState.jumpOff;//Sets state to jump off
            _rb.gravityScale = 2;
        }
    }

    //Runs the player death functionality stored else where
    void KillPlayer()
    {
        _pStateCont.PlayerDeath();//Runs player death on player state controller
    }
}
