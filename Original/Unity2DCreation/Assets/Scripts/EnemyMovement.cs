using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Enemy movement class moves an enemy back and forth similar to a Goomba from Mario games
public class EnemyMovement : MonoBehaviour
{
    //Public variables
    //Both could be private and serialized
    public float moveSpeed = 1; //Speed enemy moves
    public bool moveLeftFirst; //Decides which way the enemy should move first

    //References
    EnemyCollisionChecks _cChecks;//To store reference to collision checks component on this game object
    Rigidbody2D _rb;//To store reference to the rigidbody component on this game object

    Vector2 _velocity;//To store the calculated velocity
    int _dir = 1;//To store the current direction of travel


    //Awake is called before start and the first frame update
    void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();//Populates the rigidbody variable (assumes there is a rigidbody component on this game object)
        _cChecks = GetComponent<EnemyCollisionChecks>();//Populates the collision checks variable (assumes there is a collisin checks component on this game object)
    }

    //Start is called before the first frame update
    void Start()
    {
        //Tests which direction the enemy should move in
        if (moveLeftFirst)
        {
            _dir = -1;//Sets movement to left
        }
        else 
        {
            _dir = 1;//Sets movement to right
        }
    }

    //Update is called once per frame from the Enemy State Controller
    public void MoveUpdate()
    {
        Movement();//Calls below movement function
    }

    //Moves game object
    void Movement() 
    {
        //Sets velocity variable
        _velocity = new Vector2(_velocity.x, _rb.velocity.y);//Updates y velocity based on physics system
        _velocity.x = moveSpeed * _dir;//Updates x velocity based upon direction

        if (_cChecks.leftBlock)//Assess if left is blocked
        {
            _dir = 1;//Sets direction to move right
        }
        else if (_cChecks.rightBlock) //Assess if right is blocked
        {
            _dir = -1;//Sets direction to move left
        }
        _rb.velocity = _velocity;//Sets ridibody velocity to the calculated velocity. This makes the object move 
    }
}
