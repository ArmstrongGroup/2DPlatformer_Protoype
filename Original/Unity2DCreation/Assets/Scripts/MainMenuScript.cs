using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine.SceneManagement;//Added and in use. Need to have access to scene managment class
using UnityEngine;//Not added (there be default) and is in use.

//Main menu script has public functions to be called via Unity Buttons in the scene
public class MainMenuScript : MonoBehaviour
{
    //Loads the first level
    public void LoadScene() 
    {
        SceneManager.LoadScene(1);//Uses scene index assigned in build manager to load the first scene
    }

    //Quits the whole application
    public void QuitGame() 
    {
        Application.Quit();//Quits the entire game
        //Will only work in build and not in the Unity Editor
    }
}
