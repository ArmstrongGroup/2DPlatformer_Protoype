using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Player controller is possibly named incorrectly 
//This is really an inputs class and should probably be named as such and attached to the game manager object in the editor
public class PlayerController : MonoBehaviour
{
    //Public variables
    //All should be gettable only 
    public float inputX = 0;
    public bool jump;
    public bool fire;
    public bool inventoryShow;

    //Update is called once per frame from the Player State Controller
    //Probably should be called from the Game Manager, this would allow for inputs to be used for more than the player character
    public void InputUpdate()
    {
        //Calls input functions
        GetXInput();
        GetJump();
        GetFire();
        GetInventory();
    }

    //Gets X input from input manager 
    void GetXInput() 
    {
        inputX = Input.GetAxisRaw("Horizontal");
    }

    //Gets jump input from input manager 
    void GetJump() 
    {
        jump = Input.GetButtonDown("Jump");
    }

    //Gets fire input from input manager 
    void GetFire() 
    {
        fire = Input.GetButtonDown("Fire1");
    }

    //Gets inventory input from input manager 
    void GetInventory()
    {
        inventoryShow = Input.GetKeyDown(KeyCode.I);
        //Should set up an input in the input manager for this
        //This would enable multiple input devices and platforms to be set up simultaneously via the input manager 
    }
}
