using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Door script enables already shut doors open
//Has option for a key
//If no key is assigned it will open off a trigger
public class DoorScript : MonoBehaviour
{
    //Public variables. These could private and serialized
    public GameObject door;//Reference to door game object
    public KeyScript key;//Reference to key

    //Private variables 
    TriggerScript _trigger;//Reference to trigger nested in this game object
    Animation _anim;//Refernce to animation on the door
    bool _doorOpen;//Stores state of the door

    // Start is called before the first frame update
    void Start()
    {
        _trigger = GetComponentInChildren<TriggerScript>();//Assigns the trigger to script found on child object (assumes there is only one of these)
        _anim = GetComponentInChildren<Animation>();//Assigns the animation to script found on child object (assumes there is only one of these)
        _trigger.myTriggerEntered += DoorInteraction;//Adds door functionality to the trigger enter delegate in the the trigger reference
        SetDoorColour();//Sets the door's to match the key's (Doom style)
    }

    //Door Interaction called on player entering the trigger
    void DoorInteraction() 
    {
        if (!_doorOpen)//Checks door is not already open 
        {
            if (key != null)//Checks if the key is not null
            {
                //Asks the key if it can be used
                //Arguably this should be a call to the inventory, not the Key
                //This would make scalability and saving the state of the game easier in future
                //At present, all keys and doors must be resolved in a single scene
                if (key.UseKey())
                {
                    OpenDoor();//Opens door
                }
            }
            else 
            {
                OpenDoor();//If no key needed, door is opened
            }
        }
    }

    //Opens the door
    void OpenDoor() 
    {
        _anim.Play();//Runs animation
        _doorOpen = true;//Stores state of the door
    }

    //Sets the door colour to match the key
    void SetDoorColour() 
    {
        if (key != null)//Tests if there is a key assigned
        {
            door.GetComponent<SpriteRenderer>().color = key.colour;//Sets door colour to that of the key
        }
    }
}
