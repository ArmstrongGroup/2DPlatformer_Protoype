using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

/*The character jump class listens to input and applies force to the character (object it is attached to).
 *This is done via Unity's physics system through a rigidbody2D component.
 *As such, this class requires other components to be attached to the character.
 *These can be seen below and are populated during Start().
 */
public class CharacterJump : MonoBehaviour
{
    //Variable available in editor (though public here, these could be private and serilized)
    public float jumpForce = 5f; //Upward force aplied to jump

    Rigidbody2D _rb; //Reference variable to Rigidbody2D
    PlayerController _cont;//Reference variable to PlayerController
    CollisionChecks _col;//Reference variable to CollisionChecks

    //Delegate so other functionality can subscribe to the jump action
    //This can enable future functionality to be added (such as animation) without the need to alter this class
    public delegate void Jumped(); //Delegate is defined
    public Jumped myJump;//Instance of delegate

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();//Rigidbody2D variable is populated (assumes Rigidbody2D component is attached to this object)
        _cont = GetComponent<PlayerController>();//PlayerController variable is populated (assumes PlayerController component is attached to this object)
        _col = GetComponent<CollisionChecks>();//CollisionChecks variable is populated (assumes CollisionChecks component is attached to this object)
    }

    //Public Update is called once per frame from PlayerStateController.
    //This enables the jump functionality to only be in use while the game is in certain states.
    public void JumpUpdate()
    {
        Jump();//Calls jump function
    }

    //Jump functionality
    void Jump() 
    {
        if (_cont.jump == true && _col.onGround == true)//Jump takes place if player is grounded and the input is true.
        {
            if (myJump != null)//Checks if the delegate has any functionality subscribed to it. 
            {
                myJump();//Runs delgate, calling all subscribed functionality.
            }

            //Sets current velocity in the y axis to 0.
            //This ensure each jump is the same as it is not adding the force to an existing one.
            //Should double jump be added, this becomes more important.
            _rb.velocity = new Vector2(_rb.velocity.x, 0f);
            _rb.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);//Applies force upwards
        }
    }
}
