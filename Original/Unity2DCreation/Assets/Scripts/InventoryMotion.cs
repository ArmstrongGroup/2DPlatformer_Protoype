using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Inventory motion hides and reveals the inventory on a key press
public class InventoryMotion : MonoBehaviour
{
    //Private variables
    RectTransform _rectTransform;//Reference to rectTransform on this object (rectTransform is the transform type used in the Unity UI/Canvas system)
    PlayerController _pCont;//Reference to rectTransform on this object (rectTransform is the transform type used in the Unity UI/Canvas system)
    bool _isHidden;//Stores state of inventory visability

    // Start is called before the first frame update
    void Start()
    {
        _rectTransform = GetComponent<RectTransform>();//Assigns variable to rectTransform (assumes there is one attached to the same object as this)
        _pCont = FindObjectOfType<PlayerController>();//Assigns variable to PlayerController (assumes there is one in the scene)
        SetInventoryPosition();//Sets the position of the inventory window
    }

    // Update is called once per frame from the GameManager class
    public void InvMotionUpdate()
    {
        //Checks for input
        if (_pCont.inventoryShow)
        {
            SetInventoryPosition();//Runs set inventory position
        }
    }

    //Called from above here
    void SetInventoryPosition() 
    {
        if (_isHidden)//Checks current state of inventory
        {
            _rectTransform.anchoredPosition = new Vector2(400, 0);//Sets new position 
            _isHidden = false;//Sets state to reflect new position
        }
        else
        {
            _rectTransform.anchoredPosition = new Vector2(-400, 0);//Sets new position 
            _isHidden = true;//Sets state to reflect new position
        }
        //_isHidden could be removed and the x position of the _rectTransform used to differentiate state
        //Or the visiable and hidden positions could be made variables to make the function more usable
    }
}
