using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Character movement class sets the velocity of character in the x axis
public class CharacterMovement : MonoBehaviour
{
    //public variables, these could be private and serialized 
    public float maxSpeed = 4f; //Maximium speed
    [Range(0.0f, 1.0f)]//Limits value of accleration and provides slider
    public float acceleration = 1f;//Acceleration variable 
    [Range(0.0f, 1.0f)]//Limits value of deceleration and provides slider
    public float deceleration = 1f;//Deceleration variable 
    [Range(0.0f, 1.0f)]//Limits value of air control and provides slider
    public float airControl = 1f;//Aircontrol variable 

    Vector2 _desiredVelocity;//Stores velocity aiming for
    Vector2 _velocity;//Stores current velocity

    //Reference variables
    PlayerController _cont;//Stores player controller so there is access to player input
    Rigidbody2D _rb;//Stores rigidbody2D so velocity can be set
    CollisionChecks _col;//Stores collision checks so ground and block detection can be accessed

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();//Populates rigidbody variable with instance on same object as this class (assumes object has rigidbody2D component)
        _cont = GetComponent <PlayerController>();//Populates player controller variable with instance on same object as this class (assumes object has PlayerController component)
        _col = GetComponent<CollisionChecks>();//Populates collision checks variable with instance on same object as this class (assumes object has CollisionChecks component)
    }

    // Update is called once per frame from the Player State Controller class
    public void MoveUpdate()
    {
        CheckBlock();//Runs check block to see if there are collisons that need to prevent movement
        SetVelocity();//Sets velocity variable. Good candidate for returning the variable by altering return type of function
        _rb.velocity = _velocity; //Sets velocity of this object to the calculated value
    }

    //Checks if there are any blocking objects been detected and clamps input
    void CheckBlock() 
    {
        //If block to the left
        if (_col.leftBlock) 
        {
            _cont.inputX = Mathf.Clamp(_cont.inputX, 0, 1);//Clamps input to prevent this object being able to stick to walls
            _velocity.x = Mathf.Clamp(_velocity.x, 0, maxSpeed);//Clamps speed to only allow motion right
        }
        if (_col.rightBlock)
        {
            _cont.inputX = Mathf.Clamp(_cont.inputX, -1, 0);//Clamps input to prevent this object being able to stick to walls
            _velocity.x = Mathf.Clamp(_velocity.x, -maxSpeed, 0);//Clamps speed to only allow motion left
        }
    }

    //Sets velocity variable
    void SetVelocity() 
    {
        _velocity = new Vector2(_velocity.x, _rb.velocity.y);//Sets veocity to value calculated
        //By setting on the following frame, this give smoother motion

        //Temp variable for storing the current multiplier for air control
        float air = 1;

        //When on the ground
        if (_col.onGround)
        {
            air = 1;//Sets air control to 1 so the player has full control
        }
        else 
        {
            air = airControl;//Sets air control to chosen proportion as a proportion
        }
        
        //If the player is inputting commands
        if (_cont.inputX != 0)
        {
            _desiredVelocity = new Vector2(_cont.inputX, 0) * (maxSpeed);//Sets the desired velocity to the max speed in requested direction based on input
            _velocity.x = Mathf.MoveTowards(_velocity.x, _desiredVelocity.x, acceleration * Time.deltaTime * 240 * air);
           //Based on acceleration and air control, increases the actual velocity towards the desired velocity
        }
        else//No input from the player
        {
            _desiredVelocity = Vector2.zero;//Sets the desired velocity to zero
            _velocity.x = Mathf.MoveTowards(_velocity.x, _desiredVelocity.x, deceleration * Time.deltaTime * 240 * air);
            //Based on deceleration and air control, decreases the actual velocity towards the desired velocity
        }

    }
}
