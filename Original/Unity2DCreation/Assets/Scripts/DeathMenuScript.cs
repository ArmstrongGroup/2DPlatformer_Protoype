using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine.SceneManagement;//Added (needed for access to scene managment) and is in use.
using UnityEngine;//Not added (there be default) and is in use.

//Death Menu Script loads scenes
//Has public functions to be called by Unity UI buttons via the canvas
//This class currently has no reusability across scenes
public class DeathMenuScript : MonoBehaviour
{
    //Loads the current scene
    public void ReLoadScene() 
    {
        SceneManager.LoadScene(1);//Loads the scene via the index assigned in the build settings 
    }

    //Loads the Menu scene
    public void QuitToMenu() 
    {
        SceneManager.LoadScene(0);//Loads the scene via the index assigned in the build settings 
    }
}
