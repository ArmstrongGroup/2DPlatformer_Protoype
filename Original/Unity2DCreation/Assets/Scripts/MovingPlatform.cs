using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Moving Platform class moves a platform through an array of waypoints
//The class is designed to be placed on a empty gameobject that is used as a folder to contain all other elements
public class MovingPlatform : MonoBehaviour
{
    //Public variables (all of these could be private and serialized
    public GameObject platform;//Stores reference to the platform that moves
    public Transform[] waypoints;//Stores an array of points that the platform moves through
    public float maxSpeed = 1;//Sets the maximium speed the platform will move at 
    public float pauseTime;//The amount of time the platform will pause when it reaches a waypoint

    //Private variables
    Transform _currentTarget; //Stores the current target from the waypoints array
    Vector2 _velocity; //Stores the current velocity of the platform
    Rigidbody2D _rb; //Stores the rigidbody2D component on the moving platform
    int _currentIndex = 0; //Stores the current index in the array that is the target

    //Stores the state of the platform
    //Currently only 2 states but this makes the extension of more states (destroyed, falling, etc) easier
    enum PlatformState { moving, paused};//Defines PlatformState type
    PlatformState _platState = PlatformState.moving;//Instantiates variable of PlatformState type


    // Start is called before the first frame update
    void Start()
    {
        _currentTarget = waypoints[0];//Sets current target to first waypoint in array
        _rb = platform.GetComponent<Rigidbody2D>();//Sets rigidbody variable to one attached to this gameobject
    }

    // Update is called once per frame fro the Game Manager class
    public void PlatformUpdate()
    {
        StateMachine();//Runs the state machine below 
    }

    void StateMachine() 
    {
        switch (_platState)//Checks player state variables current value
        {
            case PlatformState.moving://When state is moving, Moving function is called
                Moving();
                break;
            case PlatformState.paused://When state is paused, no functionality is called

                break;
            default://Catches any other value of state
                //Could add a log message here to communicate something is incorrect as this should never be called
                break;
        }
    }

    //Moving function moves the platform to the target 
    void Moving() 
    {
        //Checks if the distance between the platform and the target is greated than 0.1 unity units (10cm)
        if (Vector2.Distance(platform.transform.localPosition, _currentTarget.localPosition) > 0.1f)
        {
            //If distance greater than 0.1, velocity is set by getting the vector between the 2 objects and clamping its magnitude
            //Clamping the magnitude ensures a more consistant speed
            //Result is mutiplied by the maxSpeed, allowing the speed to be controlled in editor
            //Time.deltaTime smoothes the motion as Update is being used
            _velocity = Vector2.ClampMagnitude(_currentTarget.localPosition - platform.transform.localPosition, 1) * (maxSpeed); // * 300 * Time.deltaTime);
        }
        else //If the distance is less than 0.1
        {
            if (_currentIndex == waypoints.Length - 1)//Check if the current target is last in the array
            {
                //When at last target
                //(need to consider that a waypoint can hold multiple positions in the array so ping pong motion is still possible with this system)
                _currentIndex = 0;//Set index back to zero
                _currentTarget = waypoints[0];//Set target back to the first target
            }
            else //When not at last target in array
            {
                _currentIndex++; //Up current index by 1
                _currentTarget = waypoints[_currentIndex];//Set target to next index
            }
            //Regardless of next target
            _velocity = Vector2.zero;//Stops platform
            _platState = PlatformState.paused;//Sets state to paused preventing motion
            Invoke("UnPause", pauseTime);//Invoke UnPause to start motion again after pauseTime has passed
        }
        _rb.velocity = _velocity;//Sets velocity of platform to above calculated value
    }

    //Sets platform to moving again
    void UnPause() 
    {
        _platState = PlatformState.moving;//Sets state to moving
    }

    //Stop function called from Game Manager
    //Stops platform motion at anypoint when game state changes
    public void Stop()
    {
        _rb.velocity = Vector2.zero;//Sets velocity to zero
    }
}
