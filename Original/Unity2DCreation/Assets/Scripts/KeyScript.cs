using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Key script disables key visuals on pickup and enables it's use
public class KeyScript : MonoBehaviour
{
    //Public variables
    public bool hasKey;//Could be just gettable
    public int numUses = 1;//Could be a serialized field (this allows the key to open multiple doors)
    public Color colour;//Could just be gettable

    //Private references
    TriggerScript _trigger;//Reference to trigger script
    Collider2D _col;//Reference to collider
    SpriteRenderer _sRend;//Reference to renderer
    Sprite _sSprite;//Reference to sprite
    Inventory _inv;//Reference to inventory


    // Awake is called before Start and the first frame update
    void Awake()
    {
        //Assigning references
        _trigger = GetComponentInChildren<TriggerScript>();//Assigns trigger variable to trigger script on child (assumes there is only one)
        _col = GetComponentInChildren<Collider2D>();//Assigns collider variable to Collider2D on child (assumes there is only one)
        _sRend = GetComponentInChildren<SpriteRenderer>();//Assigns renderer variable to SpriteRenderer on child (assumes there is only one)
        _inv = FindObjectOfType<Inventory>();//Assigns inventory variable to Inventory in the scene (assumes there is only one)

        //Assign data to pass to inventory on pickup
        colour = _sRend.color;//Sets colour variable to chosen colour in renderer
        _sSprite = _sRend.sprite;//Sets sprite to one chosen in renderer

        //Adds KeyPicked function to entered delegate in trigger reference
        _trigger.myTriggerEntered += KeyPicked;
    }

    //Run when trigger is entered
    void KeyPicked() 
    {
        if (_inv.AddToInv(gameObject, _sSprite, colour) == true)//Checks inventory has space and passes data
        {
            //When key is added to inventory
            _trigger.enabled = false;//Trigger is turned off
            _col.enabled = false;//Collider is turned off
            _sRend.enabled = false;//Renderer (visuals) are turned of
            hasKey = true;//Has key set to true, storing the state of the key
            //A bettter inventory system could lead to hasKey becoming redundent
        }
    }

    //Allows key dependant functionality to run on other objects if returns true
    public bool UseKey() 
    {
        if (hasKey)//Checks the key has been collected  
        {
            numUses--;//Reduces the number of uses
            if (numUses <= 0)//If total uses met
            {
                hasKey = false;//Prevents future use
                _inv.RemoveFromInv(gameObject);//Removes from inventory
            }
            return true;//Returns true allowing key to be used
        }
        return false;//Returns false preventing the key to be used
    }
}
