using System.Collections;//Not needed (there be default) and could be removed.
using System.Collections.Generic;//Not needed (there be default) and could be removed.
using UnityEngine;//Not added (there be default) and is in use.

//Projectile class moves bullets shot by the player
//Currently not using poolling, but is a ggod candidate as it would prevent the some assigning of memory and garbage collection during gameplay
public class Projectile : MonoBehaviour
{
    //Reference variable to store the rigidboby attached to this gameobject
    Rigidbody2D _rb;

    //Start is called before the first frame update
    //As instances of bullets are spawned at run time, start is used to both assign variables and run the gameplay behaviour
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();//Assigns reference to rigidbody2D 
        _rb.AddForce(transform.right * 10, ForceMode2D.Impulse); //Applies force to through this object right based upon its orientation (using local space)
        Physics2D.IgnoreLayerCollision(3, 9);//Sets ignore collisions between the player and player projectile layers
        //Should not be ignoring collisons here. This means at happens on every bullet spawn when it only needs to be called once
        Destroy(gameObject, 10);//Destroys this game object after 10 seconds
    }

    //Detects collision with other objects
    private void OnCollisionEnter2D()
    {
        Destroy(gameObject);//Instantly destroys this game object, ending all functionality on this instance
    }
}
