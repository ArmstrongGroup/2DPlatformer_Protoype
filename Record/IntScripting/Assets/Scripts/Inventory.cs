using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

//Inventory for storing game objects
public class Inventory : MonoBehaviour
{
    //Exposed in editor
    [SerializeField]
    List<Image> _imageSlots;//Refernces to inventory images (visual representation)
    [SerializeField]
    List<GameObject> _inventorySlots;//Actual game object slots

    //Adds objects to inveltory
    public bool AddToInv(GameObject obj, Sprite iconSprite, Color iconColour) //Parameters pass object to add to inventory
    {
        for (int i = 0; i < _inventorySlots.Count; i++)//Checks each slot in turn  
        {
            if (_inventorySlots[i] == null) //Checks if slot at index is empty
            {
                //Adds object if slot is empty
                _inventorySlots[i] = obj;//Adds object
                _imageSlots[i].sprite = iconSprite;//Sets icon in visual
                _imageSlots[i].color = iconColour;//Sets colour in visual
                //Returns true when a slot is filled by a new object
                return true;
            }
        }
        //Returns false if no slots = null (inventory is full)
        return false;
    }

    //Removes object passed to it
    public void RemoveFromInv(GameObject obj)//Takes object to remove 
    {
        for (int i = 0; i < _inventorySlots.Count; i++)//Checks each slot in turn 
        {
            if (_inventorySlots[i] == obj)//Checks if index contains object to remove
            {
                _inventorySlots[i] = null;//Removes object
                _imageSlots[i].sprite = null;//Clears visual sprite
                _imageSlots[i].color = Color.clear;//Clears colour to make sprite invisible

                InvetoryReset();//Resets inventory slots to move space to the bottom
                break;//Prevents future iterations of the loop
            }
        }

    }

    //Moves inventory space to the bottom
    void InvetoryReset() 
    {
        for (int i = 0; i < _inventorySlots.Count; i++)//Checks each index in turn
        {
            if (_inventorySlots[i] == null)//Checks if index is empty
            {
                if (i != _inventorySlots.Count - 1)//Checks this is not the last index and therefore there are none beyond it to move 
                {
                    _inventorySlots[i] = _inventorySlots[i + 1];//Moves next index object to this one
                    _imageSlots[i].sprite = _imageSlots[i + 1].sprite;//Move next visual sprite to this index
                    _imageSlots[i].color = _imageSlots[i + 1].color;//Move next visual colour to this index

                    //Prevents duplicate images by clearing above copied visuals and object
                    _inventorySlots[i + 1] = null;//Clears object moved to current index from old
                    _imageSlots[i + 1].sprite = null;//Clears sprite moved to current index from old
                    _imageSlots[i + 1].color = Color.clear;//Clears colour moved to current index from old
                }
            }
        }
    }
}
