using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Enables player to shoot 
public class PlayerShoot : MonoBehaviour
{
    //Exposed in editor
    [SerializeField]
    GameObject _bullet;

    //Private variables
    int _shootDir = 1;
    Vector3 _projectileRot = Vector3.zero;
    Vector3 _reverseRot = new Vector3(0,0,180);

    //Properties
    public bool HasGun { set { _hasGun = value; } }//Settable property
    bool _hasGun;//Private value for property

    //References
    PlayerController _pCont;

    // Start is called before the first frame update
    void Start()
    {
        _pCont = GetComponent<PlayerController>();//Populates Player Controller variable 
    }

    // Update is called once per frame
    public void ShootUpdate()
    {
        if (_hasGun)//Checks if player has gun
        {
            UpdateDirection();//Runs direction update
            Shoot();//Runs shooting functionality
        }
    }

    //Checks player input for facing direction
    void UpdateDirection() 
    {
        //Checks if facing right
        if (_pCont.InputX > 0)
        {
            _shootDir = 1; //Sets direction to positive
            _projectileRot = Vector3.zero; //Sets rotation to zero
        }
        else if (_pCont.InputX < 0) //Checks if facing left
        {
            _shootDir = -1; //Sets direction to negative
            _projectileRot = _reverseRot; //Sets rotation to 180
        }
    }

    //Enables shoot functionality 
    void Shoot() 
    {
        if (_pCont.Fire) //Checks input for fire
        {
            //Creates bullet at location next to player character with correct rotation
            Instantiate(_bullet, transform.position + new Vector3(0.35f * _shootDir, 0.5f, 0), Quaternion.Euler(_projectileRot));
        }
    }
}
