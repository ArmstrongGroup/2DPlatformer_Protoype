using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Assess objects hitting this one using Box Cast 2D
public class CollisionChecks : MonoBehaviour
{
    //Getable variables
    [SerializeField]
    [Range(0.0f, 1.0f)]
    float _amountVelAdded = 0.75f;
    public bool OnGround { get { return _onGround; } }
    bool _onGround; //Stores state of player on ground

    //Setable in editor
    [SerializeField]
    Vector2 _collSize = new Vector2(0.5f, 1.0f);//Stores size of collider (could be automated through bounding box)
    [SerializeField]
    LayerMask _collLayerMask;//Stores layer of objects to collide with the player
    
    //Private variables
    RaycastHit2D[] _hitters; //Array storing all hits with player on given frame
    float _midPoint; //Stores mid point of player
    Vector2 _tenPercent;//Stores ten percent of colllider size
    float _initialGrav;

    Rigidbody2D _rb;
    CharacterJump _cJump;
    Collider2D _col;
    PlayerStateController _pStateCon;
    //Platform references and information
    Rigidbody2D _platRB;
    Collider2D _platCOL;
    Vector2 _addVelocity;

    enum PlatformState { noPlatform, onPlatform, leftPlatform, jumpOff};
    PlatformState _platState = PlatformState.noPlatform;

    // Start is called before the first frame update
    void Start()
    {
        _midPoint = _collSize.y / 2;//Seting midpoint
        _tenPercent = new Vector2(_collSize.x / 10, _collSize.y / 10);//Setting ten percent value
        _collSize = _tenPercent + _collSize;//Adding ten percent to collision size
        _rb = GetComponent<Rigidbody2D>();
        _cJump = GetComponent<CharacterJump>();
        _col = GetComponent<Collider2D>();
        _pStateCon = GetComponent<PlayerStateController>();
        _initialGrav = _rb.gravityScale;

        _cJump.myJump += ReleasePlayer;

        Physics2D.IgnoreLayerCollision(3, 8);
    }

    // Update is called once per frame
    public void CollisionUpdate()
    {
        Collisions();//Runs collision checks
        VelocityUpdate();
    }

    void Collisions() 
    {
        //Stores all objects that overlay with player
        _hitters = Physics2D.BoxCastAll(new Vector2(transform.position.x, transform.position.y + _midPoint), _collSize, 0f, Vector2.zero, 0f, _collLayerMask, -0.1f, 0.1f);

        _onGround = false; //Sets default value to false

        if (_hitters.Length > 0)//Check if anything has hit the player 
        {
            for (int i = 0; i < _hitters.Length; i++)//Loops through all hits 
            {
                if (_hitters[i].collider.gameObject.layer == 0)//Hits Default layer
                {
                    if (!_onGround)//Checks that player is not already assessed as on ground from prior hit 
                    {
                        if (_hitters[i].point.y <= transform.position.y)//Checks hit is at player feet
                        {
                            _onGround = true;//Sets on ground to try
                            //break;//Ends loop
                        }
                    }
                }
                else if (_hitters[i].collider.gameObject.layer == 6)//Hits Hazard layer
                {
                    KillPlayer();
                }
                else if (_hitters[i].collider.gameObject.layer == 7)//Hits Enemy layer
                {
                    //KillPlayer();

                    if (_hitters[i].collider.gameObject.transform.position.y > (transform.position.y - 0.4f))
                    {
                        KillPlayer();
                    }
                    else
                    {
                        _hitters[i].collider.gameObject.BroadcastMessage("Die");
                        _rb.velocity = Vector2.zero;
                        _rb.AddForce(transform.up * 7.5f, ForceMode2D.Impulse);
                    }
                }
            }
        }
        else if (NoPlatform()) 
        {
            _platState = PlatformState.leftPlatform;
            _rb.gravityScale = _initialGrav;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("MovingPlatform"))
        {
            _platCOL = collision.gameObject.GetComponent<Collider2D>();

            if (_platCOL.transform.position.y < transform.position.y) 
            {
                _platState = PlatformState.onPlatform;
                _platRB = collision.gameObject.GetComponent<Rigidbody2D>();
                _rb.gravityScale = 0;
            }
        }
        else 
        {
            _platState = PlatformState.noPlatform;
            _addVelocity = Vector2.zero;
        }
    }

    bool NoPlatform() 
    {
        if (_platCOL != null) 
        {
            if ((_platCOL.bounds.extents.x + _col.bounds.extents.x) + _platCOL.transform.position.x < transform.position.x) 
            {
                _platCOL = null;
                return true;
            }
            else if ((_platCOL.transform.position.x - _platCOL.bounds.extents.x) - _col.bounds.extents.x > transform.position.x)
            {
                _platCOL = null;
                return true;
            }
        }
        return false;
    }

    void VelocityUpdate() 
    {
        switch (_platState) 
        {
            case PlatformState.leftPlatform:
                _rb.velocity = _rb.velocity + _addVelocity;
                break;
            case PlatformState.onPlatform:
                _addVelocity.x = _platRB.velocity.x;
                _onGround = true;
                _rb.velocity = new Vector2(_rb.velocity.x + _addVelocity.x, _platRB.velocity.y);
                break;
            default:
                break;
        }
    }

    void ReleasePlayer() 
    {
        if (_platState == PlatformState.onPlatform) 
        {
            _platState = PlatformState.leftPlatform;
            _addVelocity.x = _addVelocity.x * _amountVelAdded;
            _rb.gravityScale = _initialGrav;
            _onGround = false;
        }
    }

    void KillPlayer() 
    {
        //Destroy(gameObject, 0.5f);
        _pStateCon.PlayerDeath();
    }
}
