using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    Animator _animator;
    SpriteRenderer _pSprite;
    PlayerController _pCont;
    CollisionChecks _cChecks;
    CharacterMovement _cMove;
    Rigidbody2D _rb;
    bool _inAir;


    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponentInChildren<Animator>();
        _pSprite = GetComponentInChildren<SpriteRenderer>();
        _pCont = GetComponent<PlayerController>();
        _cChecks = GetComponent<CollisionChecks>();
        _cMove = GetComponent<CharacterMovement>();
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Grounded();
        SetFacingDir();
        RunAnim();
        JumpAnim();
        
    }

    void SetFacingDir() 
    {
        if (_pSprite.flipX && _pCont.InputX > 0)
        {
            _pSprite.flipX = false;
        }
        else if (!_pSprite.flipX && _pCont.InputX < 0) 
        {
            _pSprite.flipX = true;
        }
    }

    void RunAnim() 
    {
        _animator.SetFloat("RunningSpeed", Mathf.Abs(_cMove.VelocityX));
    }

    void JumpAnim() 
    {
        if (_pCont.Jump && _cChecks.OnGround)
        {
            _animator.SetTrigger("Jump");
            _inAir = true;
        }
    }

    void Grounded() 
    {
        if (_cChecks.OnGround && _inAir && _rb.velocity.y <= 0) 
        {
            _animator.SetTrigger("Grounded");
            _inAir = false;
        }
    }

    public void Death() 
    {
        _animator.SetTrigger("Death");
    }
}
