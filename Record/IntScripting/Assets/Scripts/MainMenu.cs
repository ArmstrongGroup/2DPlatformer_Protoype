using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

//Main menu functionality
//Public functions so UI buttons can use them
public class MainMenu : MonoBehaviour
{
    //Loads scene with index 1 in build settings
    public void LoadScene() 
    {
        SceneManager.LoadScene(1);//Uses scene index to load
    }

    //Quits application. Only works in build
    public void QuitGame() 
    {
        Application.Quit();//Quits entire game
    }
}
