using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Throws object with force
public class Projectile : MonoBehaviour
{
    //Exposed in editor
    [SerializeField]
    float _force = 10;

    //Reference
    Rigidbody2D _rb;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>(); //Populates rigidbody variable
        _rb.AddForce(transform.right * _force, ForceMode2D.Impulse);//Applies force in the positive x of the objects local space
        Destroy(gameObject, 5);//Destroys this object after 5 seconds
    }

    //Is called on collision with 2D object
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.CompareTag("Enemy"))//Checks if object collided with enemy 
        {
            col.collider.BroadcastMessage("Die");//If enemy is hit, Die message is sent to run Die() function
        }
        Destroy(gameObject);//Destroys this object with no delay
    }
}
