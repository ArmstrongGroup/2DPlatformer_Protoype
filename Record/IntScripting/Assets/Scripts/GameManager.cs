using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Game manager runs all functionality in the game and controls states of play
public class GameManager : MonoBehaviour
{
    //Public game state
    public enum GameState { menu, inPlay, playerDead, playerDied, reset};
    public GameState _gState = GameState.inPlay;

    //Reference variable
    PlayerStateController _pStateCont;

    //Private lists
    List<EnemyStateController> _enemies = new List<EnemyStateController>();
    List<MovingPlatform> _movingPlatforms = new List<MovingPlatform>();
    //References to UI 
    GameObject _deathUI;
    GameObject _playUI;

    // Start is called before the first frame update
    void Start()
    {
        _pStateCont = FindObjectOfType<PlayerStateController>();//Assigns player controller

        //Assigning UI via their name
        _deathUI = GameObject.Find("DeathUI");
        _playUI = GameObject.Find("PlayUI");

        //Turning off death UI
        _deathUI.SetActive(false);

        //Turning mouse off
        //UNITY BUG - does not work in editor if viewport is maximised on play but works in build and focused play
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        FindEnemies();//Finds all enemy objects in the scene
        FindPlatforms();//Finds all the platforms in the scene
    }

    //Function finds enemies based on tags
    void FindEnemies() 
    {
        GameObject[] enems = GameObject.FindGameObjectsWithTag("Enemy");//Stores enemy game objects in temp array

        //Loops through array
        foreach (GameObject obj in enems) 
        {
            _enemies.Add(obj.GetComponent<EnemyStateController>());//Adds each item's component to the list
        }
    }

    //Function finds platforms based on tags
    void FindPlatforms() 
    {
        GameObject[] plats = GameObject.FindGameObjectsWithTag("MovingPlatform");//Stores platform game objects in temp array

        foreach (GameObject obj in plats)
        {
            _movingPlatforms.Add(obj.GetComponentInParent<MovingPlatform>());// Adds each item's parent component to the list
        }
    }

    // Update is called once per frame
    void Update()
    {
        StateMachine();
    }

    //Runs game states
    void StateMachine() 
    {
        switch (_gState) 
        {
            case GameState.inPlay://Playing game
                RunPlatforms();//Runs platforms first so their velocity is established
                _pStateCont.PlayerUpdate();//Runs Player
                RunEnemies();//Runs Enemies
                break;
            case GameState.playerDied://Runs when player dies
                StopEnemies();//Stops enemies 
                StopPlatforms();//Stops platforms 
                _gState = GameState.playerDead;//Moves into next state. Ensures playerDied is only 1 frame
                break;
            case GameState.playerDead:
                //Holding state
                break;
            case GameState.reset:
                //Frees mouse
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.Confined;
                //Shows death UI and hides player UI
                _playUI.SetActive(false);
                _deathUI.SetActive(true);
                //Changes state
                _gState = GameState.menu;
                break;
            case GameState.menu:
                //Holding state
                break;
            default:

                break;

        }
    }

    //Runs enemy functionality
    void RunEnemies() 
    {
        if (_enemies != null)//Checks there are enemies to run 
        {
            for (int i = 0; i < _enemies.Count; i++) //Loops through all active enemies
            {
                if (_enemies[i] == null)//Checks enemy has not been destroyed
                {
                    _enemies.Remove(_enemies[i]);//If destroyed, removes from list
                }
                else 
                {
                    _enemies[i].EnemyUpdate();//If active, runs enemy
                }
            }
        }
    }

    //Stops enemy functionality
    void StopEnemies()
    {
        if (_enemies != null)//Checks there are enemies to stop
        {
            for (int i = 0; i < _enemies.Count; i++)//Loops through all active enemies
            {
                if (_enemies[i] == null)//Checks enemy has not been destroyed
                {
                    _enemies.Remove(_enemies[i]);//If destroyed, removes from list
                }
                else
                {
                    _enemies[i].Stop();//If active, stops enemy
                }
            }
        }
    }

    //Runs platform functionality
    void RunPlatforms()
    {
        if (_movingPlatforms != null)//Checks there are platforms to run
        {
            for (int i = 0; i < _movingPlatforms.Count; i++)//Loops through all active platforms
            {
                if (_movingPlatforms[i] == null)// Checks platform has not been destroyed
                {
                    _movingPlatforms.Remove(_movingPlatforms[i]);//If destroyed, removes from list
                }
                else
                {
                    _movingPlatforms[i].PlatformUpdate();//If active, runs platform
                }
            }
        }
    }
    //Stops platform functionality
    void StopPlatforms()
    {
        if (_movingPlatforms != null)//Checks there are platforms to stop
        {
            for (int i = 0; i < _movingPlatforms.Count; i++)//Loops through all active platforms
            {
                if (_movingPlatforms[i] == null)// Checks platform has not been destroyed
                {
                    _movingPlatforms.Remove(_movingPlatforms[i]);//If destroyed, removes from list
                }
                else
                {
                    _movingPlatforms[i].Stop();//If active, stops platform
                }
            }
        }
    }

    //Enables state change from other scripts
    public void SetState(GameState state)//Takes parameter  
    {
        _gState = state;//Sets state to given parameter
    }
}
