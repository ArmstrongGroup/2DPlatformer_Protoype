using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Collision checks with enemy
public class EnemyCollisionChecks : MonoBehaviour
{
    //Getable variables
    public bool OnGround { get { return _onGround; } }
    bool _onGround; //Stores state of object on ground
    public bool LeftBlock { get { return _leftBlock; } }
    bool _leftBlock; //Stores state of object blocked left
    public bool RightBlock { get { return _rightBlock; } }
    bool _rightBlock; //Stores state of object blocked right

    //Setable in editor
    [SerializeField]
    Vector2 _collSize = new Vector2(1.0f, 0.5f);//Stores size of collider (could be automated through bounding box)
    [SerializeField]
    LayerMask _collLayerMask;//Stores layer of objects to collide with the object

    //Private variables
    RaycastHit2D[] _hitters; //Array storing all hits with object on given frame
    float _midPoint; //Stores mid point of object
    Vector2 _tenPercent;//Stores ten percent of colllider size


    // Start is called before the first frame update
    void Start()
    {
        _midPoint = _collSize.y / 2;//Seting midpoint
        _tenPercent = new Vector2(_collSize.x / 10, _collSize.y / 10);//Setting ten percent value
        _collSize = _tenPercent + _collSize;//Adding ten percent to collision size
    }

    // Update is called once per frame
    public void CollisionUpdate()
    {
        Collisions();//Runs collision checks
    }

    void Collisions()
    {
        //Stores all objects that overlay with player
        _hitters = Physics2D.BoxCastAll(new Vector2(transform.position.x, transform.position.y + _midPoint), _collSize, 0f, Vector2.zero, 0f, _collLayerMask, -0.1f, 0.1f);

        _onGround = false; //Sets default value to false
        _rightBlock = false; //Sets default value to false
        _leftBlock = false; //Sets default value to false

        if (_hitters.Length > 0)//Check if anything has hit this object 
        {
            for (int i = 0; i < _hitters.Length; i++)//Loops through all hits 
            {
                if (_hitters[i].collider.gameObject != gameObject)
                {
                    if (!_onGround)//Checks that player is not already assessed as on ground from prior hit 
                    {
                        if (_hitters[i].point.y <= transform.position.y)//Checks hit is at object feet
                        {
                            _onGround = true;//Sets on ground to try
                        }
                    }
                    if (!_rightBlock)
                    {
                        if (_hitters[i].point.x < transform.position.x)//Checks if hit is on the left
                        {
                            //Ignores left hits
                        }
                        else if (_hitters[i].point.y >= transform.position.y && _hitters[i].point.y < (transform.position.y + (_midPoint * 2)))//Checks hit is between top and bottom 
                        {
                            _rightBlock = true;
                        }
                    }
                    if (!_leftBlock)
                    {
                        if (_hitters[i].point.x > transform.position.x)//Checks if hit is on the right
                        {
                            //Ignores right hits
                        }
                        else if (_hitters[i].point.y >= transform.position.y && _hitters[i].point.y < (transform.position.y + (_midPoint * 2)))//Checks hit is between top and bottom
                        {
                            _leftBlock = true;
                        }
                    }
                }
            }
        }
    }

}
