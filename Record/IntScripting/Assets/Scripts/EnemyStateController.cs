using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class to run enemies
public class EnemyStateController : MonoBehaviour
{
    //Private variables
    bool _alive = true;//Stores state of enemy (alive or dead)
    bool _falling;//Stores state of enemy falling

    //Reference variables
    EnemyMovement _eMove;
    EnemyCollisionChecks _cChecks;
    Rigidbody2D _rb;
    Collider2D _col;

    // Start is called before the first frame update
    void Start()
    {
        _eMove = GetComponent<EnemyMovement>();//Stores reference to movement
        _cChecks = GetComponent<EnemyCollisionChecks>();//Stores reference to collision checks
        _rb = GetComponent<Rigidbody2D>();//Stores reference to rigidbody 
        _col = GetComponent<Collider2D>();//Stores reference to collision
    }

    // Update is called once per frame
    public void EnemyUpdate()
    {
        StateMachine();//Runs state machine
    }

    //Enemy actions based on state
    void StateMachine() 
    {
        if (_alive) //Check if enemy is alive
        {
            _eMove.MoveUpdate();//Moves enemy
            _cChecks.CollisionUpdate();//Runs enemy collision checks
            FallCheck();//Tests if enemy is falling
        }
    }

    //Checks to see if enemy has fallen of the world
    void FallCheck() 
    {
        if (!_cChecks.OnGround && !_falling)//Checks if enemy should be set to falling 
        {
            _falling = true;//Sets to falling 
            Invoke("Death", 2);//Sets death to take place after 2 seconds
        }
        if (_cChecks.OnGround && _falling)//Checks if enemy should be set to NOT falling 
        {
            _falling = false;//Sets to not falling
            CancelInvoke("Death");//Stops death from taking place
        }

    }

    //Function to change enemy state to dead
    public void Die() 
    {
        _alive = false;//Sets enemy to dead
        _col.enabled = false;//Prevents collisions with other objects
        _rb.velocity = Vector2.zero;//Stops all movement
        _rb.AddForce(transform.up * 5, ForceMode2D.Impulse);//Throws up in the air
        Invoke("Death", 2);//Sets death to take place after 2 seconds
    }

    //Final death
    void Death() 
    {
        Destroy(gameObject);//Destroys enemy object 
    }

    //Stops motion
    public void Stop()
    {
        _rb.velocity = Vector2.zero;//Stops motion
        _rb.simulated = false;//Stops physics
        CancelInvoke();//Stops death if already called
    }
}
