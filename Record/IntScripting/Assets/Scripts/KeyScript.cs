using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Key functionality
public class KeyScript : MonoBehaviour
{
    //Exposed in editor
    [SerializeField]
    int _numUses = 1;//Decides how many times the key can be used

    //Property storing the key colour. Doors and keys match colour
    public Color Colour { get { return _colour; } }
    Color _colour;//Private field for property

    //Stores state of key
    bool _hasKey;

    //Reference variables
    TriggerScript _trigger;
    Collider2D _col;
    SpriteRenderer _sRend;
    Inventory _inv;
    Sprite _sprite;

    // Awake is called before the first frame update and start
    //Used over start to ensure door can retrieve correct colour
    void Awake()
    {
        //Assigns references
        _trigger = GetComponentInChildren<TriggerScript>();
        _col = GetComponentInChildren<Collider2D>();
        _sRend = GetComponentInChildren<SpriteRenderer>();
        _inv = FindAnyObjectByType<Inventory>();
        //Assigns variables from renderer
        _colour = _sRend.color;
        _sprite = _sRend.sprite;
        
        //Subscribes function to trigger delegate
        _trigger.myTrigEntered += KeyPicked;
    }

    //Unsubscribes function from trigger delegate
    private void OnDisable()
    {
        _trigger.myTrigEntered -= KeyPicked;
    }

    //Function called from trigger entry
    void KeyPicked() 
    {
        if (_inv.AddToInv(gameObject, _sprite, _colour))//Checks if inventory has space. If so sends parameters
        {
            _trigger.enabled = false;//Turns of trigger
            _col.enabled = false;//Turns of collider
            _sRend.enabled = false;//Turns of renderer
            _hasKey = true;//Changes state of key to picked up
        }
    }

    //Called when key is used
    public bool UseKey() 
    {
        if (_hasKey)//Checks if player has key 
        {//Runs if player has key
            _numUses--;//Uses the key 
            if (_numUses <= 0)//Checks is key is exhausted 
            {
                //Runs when there are no more uses left on the key
                _hasKey = false;//Sets has key to false
                _inv.RemoveFromInv(gameObject);//Removes key from inventory
            }
            return true;//Returns to tell the door to open
        }
        return false;//If player doesn't have the key, false is retuned
    }
}
