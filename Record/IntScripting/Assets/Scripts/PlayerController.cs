using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Gets inputs to be used by the player sprite
public class PlayerController : MonoBehaviour
{
    //Getable variables
    public float InputX { get { return _inputX; } }
    float _inputX = 0;

    public bool Jump { get { return _jump; } }
    bool _jump;

    public bool Fire { get { return _fire; } }
    bool _fire;

    // Update is called once per frame
    public void InputUpdate()
    {
        //Calls input functions
        GetXInput();
        GetJump();
        GetFire();
    }

    //Gets x input from input manager
    void GetXInput()
    {
        _inputX = Input.GetAxisRaw("Horizontal");//Uses raw input with no smoothing
    }

    //Gets jump input from input manager
    void GetJump() 
    {
        _jump = Input.GetButtonDown("Jump");
    }

    //Gets fire input from input manager
    void GetFire()
    {
        _fire = Input.GetButtonDown("Fire1");
    }
}
