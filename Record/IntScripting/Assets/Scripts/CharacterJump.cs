using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterJump : MonoBehaviour
{
    //Setable in editor
    [SerializeField]
    float _jumpForce = 5f;

    //References
    Rigidbody2D _rb; //This objects rigidbody
    PlayerController _cont; //This objects controller
    CollisionChecks _coll; //This objects collision checks

    public delegate void Jumped();
    public Jumped myJump;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>(); //Assigns rigidbody to one on this object
        _cont = GetComponent<PlayerController>(); //Assigns player controller to one on this object
        _coll = GetComponent<CollisionChecks>(); //Assigns collision checks to one on this object
    }

    // Update is called once per frame
    public void JumpUpdate()
    {
        Jump();//Runs jump functionality
    }

    void Jump() 
    {
        if (_cont.Jump == true && _coll.OnGround == true) //Checks for input and if player is on the ground
        {
            if (myJump != null) 
            {
                myJump();
            }
            _rb.AddForce(transform.up * _jumpForce, ForceMode2D.Impulse); //Applies force upwards to jump the player
        }
    }
}
