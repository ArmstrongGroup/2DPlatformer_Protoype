using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    //Setable in editor
    [SerializeField]
    float maxSpeed = 4f;

    [SerializeField]
    [Range(0.0f, 1.0f)]
    float acceleration = 1f;

    [SerializeField]
    [Range(0.0f, 1.0f)]
    float deceleration = 1f;

    [SerializeField]
    [Range(0.0f, 1.0f)]
    float airControl = 1f;

    //Private variables
    float _desiredVelocityX; //Stores velocity target
    public float VelocityX { get { return _velocityX; } }
    float _velocityX;//Stores current velocity
    float _inX = 0;//Stores current input value

    //Refernce variables
    PlayerController _cont;//Stores reference to inputs
    Rigidbody2D _rb;//Stores reference to the player controller
    CollisionChecks _coll;//Stores reference to collision checks

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>(); // Assigns rigidbody to one on this object
         _cont = GetComponent<PlayerController>(); //Assigns player controller to one on this object
        _coll = GetComponent<CollisionChecks>(); //Assigns collision checks to one on this object
    }

    // Update is called once per frame
    public void MoveUpdate()
    {
        _inX = _cont.InputX; //Sets input to one from player controller
        SetVelocity();//Runs x velocity calculations
        _rb.velocity = new Vector2(_velocityX, _rb.velocity.y); //Applies x velocity and leaves y to the physics engine
    }

    void SetVelocity() 
    {
        float air = 1;//Temp variable to store air control value

        if (_coll.OnGround)//Checks if on ground
        {
            air = 1;//Sets air to one so causes no loss in control
        }
        else 
        {
            air = airControl;//Sets air control to value set in editor. Below 1 limits player control
        }

        if (_inX != 0)//Checks player input is occuring
        {
            _desiredVelocityX = _inX * maxSpeed;//Sets desired velocity to max speed in correct direction
            _velocityX = Mathf.MoveTowards(_velocityX, _desiredVelocityX, acceleration * Time.deltaTime * 240 * air);//Sets velocity bassed on acceleration
        }
        else //No player input
        {
            _desiredVelocityX = 0;//Sets target velocity to 0
            _velocityX = Mathf.MoveTowards(_velocityX, _desiredVelocityX, deceleration * Time.deltaTime * 240 * air);//Sets velocity bassed upon deceleration
        }
    }
}
