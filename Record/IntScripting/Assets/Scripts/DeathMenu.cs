using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

//Death menu funtionality
//Public functions so UI buttons can use them
public class DeathMenu : MonoBehaviour
{
    //Loads level based on index in build settings
    public void ReLoadScene() 
    {
        SceneManager.LoadScene(1);//Loads level 1
    }

    //Loads level based on index in build settings
    public void QuitToMenu()
    {
        SceneManager.LoadScene(0);//Loads main menu
    }
}
