using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Trigger script for trigger prefab
public class TriggerScript : MonoBehaviour
{
    //Exposed in editor
    [SerializeField]
    string _effectedTag;

    //Gettable property
    public bool InTrigger { get { return _inTrigger; } }
    bool _inTrigger = false;

    //Public delegates
    public delegate void TriggerEntered();
    public TriggerEntered myTrigEntered;

    public delegate void TriggerExited();
    public TriggerExited myTrigExited;

    //Runs on trigger collider being entered
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(_effectedTag)) //Checks if object entering is tagged with effected tag
        {
            _inTrigger = true;//Sets in trigger to true

            if (myTrigEntered != null) //Checks delegate is not empty 
            {
                myTrigEntered(); //Runs delegate
            }
        }
    }

    //Runs on trigger collider being exited
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag(_effectedTag)) //Checks if object exited is tagged with effected tag
        {
            _inTrigger = false;//Sets in trigger to false

            if (myTrigExited != null) //Checks delegate is not empty 
            {
                myTrigExited(); //Runs delegate
            }
        }
    }
}
