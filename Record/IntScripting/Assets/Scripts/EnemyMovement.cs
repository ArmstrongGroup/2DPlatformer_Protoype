using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class for base enemy motion (back and forth)
public class EnemyMovement : MonoBehaviour
{
    //Exposed in editor
    [SerializeField]
    float _moveSpeed = 1;
    [SerializeField]
    bool _moveLeftFirst = false;

    //References
    Rigidbody2D _rb; 
    EnemyCollisionChecks _enemyColChecks;

    //Private variables
    Vector2 _velocity;
    int _dir = 1;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>(); //Stores reference to rigidbody
        _enemyColChecks = GetComponent<EnemyCollisionChecks>(); //Stores reference to collision checks
    }

    // Start is called before the first frame update
    void Start()
    {
        if (_moveLeftFirst)//Checks if move left first is requested
        {
            _dir = -1;//Sets direction to left (minus)
        }
        else 
        {
            _dir = 1;//Sets direction to right (positive)
        }
    }

    // Public Update is called once per frame from other script
    public void MoveUpdate()
    {
        Movement();//Calls motion   
    }

    //Motion functionality
    void Movement() 
    {
        if (_enemyColChecks.RightBlock)//Checks if blocked right
        {
            _dir = -1;//Sets motion to moving left
        }
        else if (_enemyColChecks.LeftBlock)//Checks if blocked left 
        {
            _dir = 1;//Sets motion to moving right
        }
        _velocity = new Vector2(_moveSpeed * _dir, _rb.velocity.y);//Stores desired movement velocity
        _rb.velocity = _velocity;//Applies velocity through rigidbody
    }
}
