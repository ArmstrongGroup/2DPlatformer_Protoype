using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Shooting pickup that enables shooting in the player character
public class ShootingPickup : MonoBehaviour
{
    //References 
    TriggerScript _trigger;
    PlayerShoot _pShoot;
    SpriteRenderer _sRend;

    // Start is called before the first frame update
    void Start()
    {
        _sRend = GetComponent<SpriteRenderer>(); //Populates Sprite Renderer variable
        _trigger = GetComponentInChildren<TriggerScript>(); //Populates trigger variable from child object
        _pShoot = FindObjectOfType<PlayerShoot>(); //Populates shoot variable from one in scene

        _trigger.myTrigEntered += PickedUp; //Subscribes PickedUp function to delegate in trigger
    }

    //Called on disabling of this object
    private void OnDisable()
    {
        _trigger.myTrigEntered -= PickedUp; //Unsubscribes PickedUp function to delegate in trigger
    }

    //Called on pick up from trigger delegate 
    void PickedUp() 
    {
        _trigger.enabled = false;//Turns off trigger
        _sRend.enabled = false;//Turns off sprite renderer
        _pShoot.HasGun = true;//Sets having the gun to true
    }

}
