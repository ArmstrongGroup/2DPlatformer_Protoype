using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Runs door functionality 
public class DoorScript : MonoBehaviour
{
    //Exposed in editor
    [SerializeField]
    GameObject _door;//Reference to the door
    [SerializeField]
    KeyScript _key;//Reference to the key

    //Reference variables
    TriggerScript _trigger;
    Animation _anim;
    //Private variables 
    bool _doorOpen;//Stores state of door


    // Start is called before the first frame update
    void Start()
    {
        //Assigns references
        _trigger = GetComponentInChildren<TriggerScript>();
        _anim = GetComponentInChildren<Animation>();
        //Subscribes function to trigger delegate
        _trigger.myTrigEntered += DoorInteraction;
        //Sets door colour
        SetDoorColour();
    }

    //Unsubscribes function to trigger delegate
    private void OnDisable()
    {        
        _trigger.myTrigEntered -= DoorInteraction;
    }

    //Checks if door sould open
    void DoorInteraction() 
    {
        if (!_doorOpen)//Checks door is closed 
        {
            if (_key != null)//Checks if door requires a key
            {
                //Checks if player has key and if it has uses left
                if (_key.UseKey()) 
                {
                    OpenDoor();//Opens door
                }
            }
            else //Opens door if no key is required
            {
                OpenDoor();//Opens door
            }
        }
    }

    //Opens door
    void OpenDoor() 
    {
        _anim.Play();//Plays animation 
        _doorOpen = true;//Sets door state to open
    }

    //Sets colour of the door
    void SetDoorColour() 
    {
        if (_key != null) //Checks if there is a key to match it to
        {
            _door.GetComponent<SpriteRenderer>().color = _key.Colour;//Matches door colour to the required key
        }
    }
}
