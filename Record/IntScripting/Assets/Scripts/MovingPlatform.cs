using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Runs platform motion
public class MovingPlatform : MonoBehaviour
{
    //Exposed in editor to assign
    [SerializeField]
    GameObject _platform; //Refernce to platform object
    [SerializeField]
    Transform[] _waypoints; //Stores waypoints
    [SerializeField]
    float _maxSpeed = 1; //Platform Speed
    [SerializeField]
    float _pauseTime; //Time platform stops at waypoint

    //Private Variables
    Vector2 _velocity; //Stores velocity to apply to platform
    int _currentIndex; //Stores the current index waypoint
    bool _paused; //Stores motion state of platform

    //Reference variables
    Transform _currentTarget; //Stores current target waypoint
    Rigidbody2D _rb; //Stores platform rigidbody

    // Start is called before the first frame update
    void Start()
    {
        _currentTarget = _waypoints[0]; //Set first target to first waypoint
        _rb = _platform.GetComponent<Rigidbody2D>(); //Gets rigidbody reference off platform
    }

    // Update is called once per frame from GameManager
    public void PlatformUpdate()
    {
        StateMachine();//Runs platform states
    }

    void StateMachine() 
    {
        if (!_paused) //Checks state
        {
            Moving(); //Runs motion when not paused
        }
    }

    //Moves platform
    void Moving() 
    {
        Vector2 dir = _currentTarget.localPosition - _platform.transform.localPosition; //Stores vector to next waypoint

        if (Vector2.Distance(_platform.transform.localPosition, _currentTarget.localPosition) > _rb.velocity.magnitude / (1 / Time.deltaTime)) //Checks distance to waypoint is greater than distance traveeled on a single frame
        {
            _velocity = dir.normalized * _maxSpeed; //Sets velocity to constant speed towards target
        }
        else //Runs when target is reached
        {
            _platform.transform.localPosition = _currentTarget.localPosition; //Sets platform position to destination
            if (_currentIndex == _waypoints.Length - 1)//Checks if final waypoint has been reached
            {
                _currentIndex = 0; //Sets current index back to start (0)
                _currentTarget = _waypoints[0];//Resets target to start (first waypoint)
            }
            else //Runs when not at final waypoint
            {
                _currentIndex++; //Ups index by 1
                _currentTarget = _waypoints[_currentIndex];//Sets target to next index
            }

            _velocity = Vector2.zero;//Stops platform
            _paused = true;//Pauses motion
            Invoke("UnPause", _pauseTime);//Starts unpause based on delay
        }

        _rb.velocity = _velocity;//Sets velocity to value assigned above
    }

    //Unpauses platform
    void UnPause() 
    {
        _paused = false; //Sets state to false, allowing motion again
    }

    //Stops platform from running
    public void Stop()
    {
        _rb.velocity = Vector2.zero;//Kills all motion
    }

}
