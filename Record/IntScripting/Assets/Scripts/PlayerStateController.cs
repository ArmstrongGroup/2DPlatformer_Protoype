using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;


//Runs all player funcionality
public class PlayerStateController : MonoBehaviour
{
    //Reference variables
    PlayerController _pControl;
    CharacterMovement _cMove;
    CharacterJump _cJump;
    PlayerShoot _pShoot;
    CollisionChecks _cChecks;
    Rigidbody2D _rb;
    Collider2D _col;
    CinemachineVirtualCamera _vCam;
    GameManager _gManager;
    AnimationController _animCont;

    //Private variables
    bool _alive = true; //Stores state of player 


    // Start is called before the first frame update
    void Start()
    {
        //Assigning references
        _pControl = GetComponent<PlayerController>(); 
        _cMove = GetComponent<CharacterMovement>();
        _cJump = GetComponent<CharacterJump>();
        _pShoot = GetComponent<PlayerShoot>();
        _cChecks = GetComponent<CollisionChecks>();
        _rb = GetComponent<Rigidbody2D>();
        _col = GetComponent<Collider2D>();
        _vCam = FindObjectOfType<CinemachineVirtualCamera>();
        _gManager = FindAnyObjectByType<GameManager>();
        _animCont = GetComponent<AnimationController>();
    }

    // Update is called once per frame from Game Manager
    public void PlayerUpdate()
    {
        if (_alive)//Checks state of player
        {
            _pControl.InputUpdate();//Runs Inputs
            _cMove.MoveUpdate();//Runs Motion
            _cJump.JumpUpdate();//Runs Jump
            _pShoot.ShootUpdate();//Runs Shooting
            _cChecks.CollisionUpdate();//Runs collisions
        }
    }

    //Kills player
    public void PlayerDeath() 
    {
        _alive = false;//Stops all functionality
        _rb.simulated = false;//Stops physics
        _col.enabled = false;//Prevents collisions
        _vCam.enabled = false;//Stops camera following
        _gManager.SetState(GameManager.GameState.playerDied);//Changes state in Game Manager
        Invoke("DeathAnim", 1f);//Starts death animation
        _animCont.Death();
    }
    //Death animation
    void DeathAnim() 
    {
        _rb.simulated = true;//Starts physics
        _rb.velocity = Vector2.zero;//Prevents any carry over of motion from game
        _rb.AddForce((transform.up * 7.5f), ForceMode2D.Impulse);//Throws player upwards
        Invoke("ResetGame", 2f);//Starts game reset
    }

    //Resets game
    void ResetGame() 
    {
        _gManager.SetState(GameManager.GameState.reset);//Changes state in game manager
        _rb.velocity = Vector2.zero;//Stops motion
        _rb.simulated = false;//Stops physics
    }

}
