using UnityEngine;

public class CharacterJump : MonoBehaviour
{
    //Editable in inspector
    [SerializeField]
    float _jumpForce = 5f;//Force applied to jump character

    //Reference variables
    Rigidbody2D _rb;//Stores ref to rigidbody (physics / motion)
    PlayerController _playerCon;//Stores ref to player controller (inputs)
    CollisionChecks _collisions;//Stores ref to collisions

    //Public delegate to be called on jump
    public delegate void Jumped();//Defines type
    public Jumped myJump; //Instance of the Jumped delegate declared

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();//Populates ref to rigidbody (physics / motion)
        _playerCon = GetComponent<PlayerController>();//Populates ref to player controller (inputs)
        _collisions = GetComponent<CollisionChecks>();//Populates ref to collisions
    }

    // Update is called once per frame
    public void JumpUpdate()
    {
        Jump();//Runs Jump functionality
    }

    void Jump() 
    {
        if(_playerCon.Jump == true && _collisions.OnGround == true)//Checks if jump button is pressed and if character is on the ground 
        {
            if(myJump != null)//Checks if delegate is not null (i.e. are there subscribing functions) 
            {
                myJump();//Runs delegate when there are subscribing functions 
            }

            _rb.velocity = new Vector2(_rb.velocity.x, 0f);//Sets current y velocity to zero (useful if double jump is created)
            _rb.AddForce(transform.up * _jumpForce, ForceMode2D.Impulse);//Applies upwards force with magnitude of the jump force variable
        }
    }
}
