using UnityEngine;

public class EnemyProjectileCollisions : MonoBehaviour
{
    //On collision enter 2d is called via the physics system
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("PlayerProjectile"))//Checks if colliding object is a projectile from the player 
        {
            BroadcastMessage("Die");//Kills this enemy
        }
    }
}
