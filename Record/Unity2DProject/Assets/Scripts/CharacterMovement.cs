using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    //Editable in inspector
    [SerializeField]
    [Range(0f, 10f)]
    float _maxSpeed = 4f;//Speed of player character x movement
    [SerializeField]
    [Range(0f, 1f)]
    float _acceleration = 1f;//Rate of change from 0 to max speed
    [SerializeField]
    [Range(0f, 1f)]
    float _deceleration = 1f;//Rate of change max speed to 0

    //Private variabls
    Vector2 _desiredVelocity;//Stores target velocity 
    Vector2 _velocity;//Stores current velocity
    float _xMotion = 0;//Stores input driving velocities

    //References
    PlayerController _playerCon;//Stores ref to player controller (inputs)
    Rigidbody2D _rb;//Stores ref to rigidbody (physics / motion)
    CollisionChecks _collisions;//Stores ref to collisions

    // Start is called before the first frame update
    void Start()
    {
        //Set references
        _playerCon = GetComponent<PlayerController>();//Populates with reference of type attached to the same game object as this class
        _rb = GetComponent<Rigidbody2D>();//Populates with reference of type attached to the same game object as this class
        _collisions = GetComponent<CollisionChecks>();//Populates with reference of type attached to the same game object as this class
    }

    // Update is called once per frame
    public void MoveUpdate()
    {
        _xMotion = _playerCon.InputX;//Stores input from player controller in variable
        CheckBlock();//Runs block check to see if input needs clamping
        SetVelocity();//Sets velocity variables
        _rb.velocity = new Vector2 (_velocity.x, _rb.velocity.y);//Applies velocity to the rigidbody
    }

    void CheckBlock() 
    {
        if (_collisions.LeftBlock)//Checks if collision has been detected to the left 
        {
            //Runs if left is blocked
            _xMotion = Mathf.Clamp(_xMotion, 0, 1);//Clamps input to prevent motion left (-x direction)
            _velocity.x = Mathf.Clamp(_velocity.x, 0, _maxSpeed);//Clamps velocity to prevent motion left (-x direction)
        }

        if (_collisions.RightBlock)//Checks if collision has been detected to the right
        {
            //Runs if right is blocked
            _xMotion = Mathf.Clamp(_xMotion, -1, 0);//Clamps input to prevent motion right (x direction)
            _velocity.x = Mathf.Clamp(_velocity.x, -_maxSpeed, 0);// Clamps velocity to prevent motion right (x direction)
        }
    }

    void SetVelocity() 
    {
        if (_xMotion != 0)//Checks player is inputing motion (pressing a key)
        {
            _desiredVelocity = new Vector2(_xMotion, 0) * _maxSpeed;//Sets desired motion to vector with magnitude of the maxspeed in correct direction
            _velocity.x = Mathf.MoveTowards(_velocity.x, _desiredVelocity.x, 
                _acceleration * Time.deltaTime * 240); //Nudges velocity closer to the desired velocity at a rate determined by the acceleration
        }
        else 
        {
            _desiredVelocity = Vector2.zero;//Sets desired zero
            _velocity.x = Mathf.MoveTowards(_velocity.x, _desiredVelocity.x, 
                _deceleration * Time.deltaTime * 240);//Nudges velocity closer to the desired velocity at a rate determined by the deceleration
        }
    }
}
