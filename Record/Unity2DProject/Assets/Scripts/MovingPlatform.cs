using System;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    //Editable in Editor
    [SerializeField]
    GameObject _platform;//Stores reference to the platform game object. Dragged and dropped in editor
    [SerializeField]
    Transform[] _waypoints;//Stores waypoints that plaform moves between
    [SerializeField]
    float _speed = 1;//Stores speed platform moves at
    [SerializeField]
    float _pauseTime = 0;//Stores time platform pauses for when waypoint is reached

    //Private variables
    Transform _currentTarget;//Stores current target platform is moving to
    Vector2 _velocity;//Stores velocity platform is moving at
    Rigidbody2D _rb;//Stores reference to rigidbody on the moving platform
    int _currentIndex = 0;//Stores current index of waypoint that is the current target
    bool _isMoving = true;//Stores the state of whether the platform is moving or not


    // Start is called before the first frame update
    void Start()
    {
        _currentTarget = _waypoints[0];//Sets current target (where the platfrom will move to) to the object at index 0 in waypoints
        _rb = _platform.GetComponent<Rigidbody2D>();//Populates the rigidbody variable with the one attached to the platform object
    }

    // Platform Update is called once per frame from the game manager
    public void PlatformUpdate()
    {
        Platform();//Runs platform
    }

    void Platform() 
    {
        if(_isMoving)//Checks if platform should be allowed to move 
        {
            Motion();//Runs the platforms motion
        }
    }

    void Motion() 
    {
        if (Vector2.Distance(_platform.transform.localPosition, _currentTarget.localPosition) > 0.1f)//Checks if the distance to the waypoint is greater than 0.1 units
        {
            Vector2 dir = (_currentTarget.localPosition - _platform.transform.localPosition);//Stores the direction to move in
            dir.Normalize();//Normalises the direction vector so it's magnitude is 1. This ensures a constant speed

            _velocity = dir * _speed;//Sets the velocity to the correct direction and speed
        }
        else //Change target to next waypoint
        {
            if (_currentIndex == _waypoints.Length - 1)//Checks if as the last waypoint
            {
                _currentIndex = 0;//Sets current index to 0
            }
            else //If not at last waypoint, move to next
            {
                _currentIndex++;//Ups the index to the next one
            }
            _currentTarget = _waypoints[_currentIndex]; // Sets current target to new index
            _velocity = Vector2.zero;//Sets velocity to 0, stopping all movement
            _isMoving = false;//Sets moving state to false (stationary)
            Invoke("UnPause", _pauseTime);//Invokes method to start motion again after pause time has alapsed
        }
        _rb.velocity = _velocity;//Sets the rigidbody's velocity to velocity variable
    }

    //public function to stop platform
    public void Stop() 
    {
        _rb.velocity = Vector2.zero;//Stops all motion of the platform
    }

    void UnPause() 
    {
        _isMoving = true;//Allows motion again
    }
}
