using UnityEngine;

public class EnemyCollisionChecks : MonoBehaviour
{
    //Editable in inspector
    [SerializeField]
    LayerMask _collisionMask;//Stores layer mask to limit collisions assessed based upon layers
    [SerializeField]
    Vector2 _collisionSize = new Vector2(0.5f, 1.0f);//Stores size of the character collider (must be set to size of game object's bounding box

    bool _onGround;//Stores state of character on ground
    public bool OnGround { get { return _onGround; } }//Enables on ground to be readable

    bool _leftBlock;//Stores state of character left block
    public bool LeftBlock { get { return _leftBlock; } }//Enables left block to be readable

    bool _rightBlock;//Stores state of character right block
    public bool RightBlock { get { return _rightBlock; } }//Enables right block to be readable

    RaycastHit2D[] _hitters;//Array storing every hit from surrounding game objects
    float _midpoint;//Stores difference in y of the game object's pivot point and the centre point

    // Start is called before the first frame update
    void Start()
    {
        _midpoint = _collisionSize.y / 2;//Sets midpoint
        Vector2 _tenPercent = new Vector2(_collisionSize.x / 10, _collisionSize.y / 10);//Stores value of 10% of collision size
        _collisionSize = _collisionSize + _tenPercent;//Increases collision size by 10%
    }

    // Update is called once per frame
    public void CollisionUpdate()
    {
        Collisions();//Runs collisions
    }

    void Collisions()
    {
        Vector2 origin = new Vector2(transform.position.x, transform.position.y + _midpoint);//Calculates the center point of the game object in world space
        _hitters = Physics2D.BoxCastAll(origin, _collisionSize, 0f, Vector2.zero, 0f, _collisionMask, -0.1f, 0.1f);//Gets objects colliding with this game object via Box Cast
        //Objects are sored in an array

        _onGround = false;//Sets on ground to false every frame
        _leftBlock = false;//Sets left block to false every frame
        _rightBlock = false;//Sets right block to false every frame

        if (_hitters.Length > 0)//Only runs if an object is hitting this game object 
        {

            for (int i = 0; i < _hitters.Length; i++)//Loops through every object hit every frame 
            {
                if (_hitters[i].collider.gameObject != gameObject)
                {
                    if (!_onGround)//Only runs if on ground is false 
                    {
                        if (_hitters[i].point.y <= transform.position.y)//Checks if hit point is below the player
                        {
                            _onGround = true;//Sets on ground to true
                        }
                    }

                    if (!_rightBlock)//Only runs if right block is false 
                    {
                        if (_hitters[i].point.x < transform.position.x)//Checks if hit is on left
                        {
                            //Hit is disregarded
                        }
                        //Checks if hit is not on the bottom of the player or the top of the player
                        else if (_hitters[i].point.y >= transform.position.y
                            && _hitters[i].point.y < (transform.position.y + (_midpoint * 2)))
                        {
                            _rightBlock = true;//Sets right block to true
                        }
                    }

                    if (!_leftBlock)//Only runs if left block is false 
                    {
                        if (_hitters[i].point.x > transform.position.x)//Checks if hit is on right
                        {
                            //Hit is disregarded
                        }
                        //Checks if hit is not on the bottom of the player or the top of the player
                        else if (_hitters[i].point.y >= transform.position.y
                            && _hitters[i].point.y < (transform.position.y + (_midpoint * 2)))
                        {
                            _leftBlock = true;//Sets left block to true
                        }
                    }
                }
            }
        }
    }
}
