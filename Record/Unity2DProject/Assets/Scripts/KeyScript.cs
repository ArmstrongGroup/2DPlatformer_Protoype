using UnityEngine;

public class KeyScript : MonoBehaviour
{
    //Editable in Inspector
    [SerializeField]
    int _numUses = 1;//Number of times the key can be used. Enables use on multiple doors.

    bool _hasKey;//Stores state of having the key
    public bool HasKey { get { return _hasKey; } }//Enables the key state to be seen

    //References
    TriggerScript _trig;//Reference to the trigger
    Collider2D _coll;//Reference to the collider
    SpriteRenderer _sRend;//Reference to the renderer
    Sprite _sprite;//Reference to the sprite
    Inventory _inv;//Reference to the inventory 
    
    Color _colour;//Stores the colour of the key


    // Start is called before the first frame update
    void Start()
    {
        _trig = GetComponentInChildren<TriggerScript>();//Gets trigger in child object
        _coll = GetComponentInChildren<Collider2D>();//Gets collider in child object
        _sRend = GetComponentInChildren<SpriteRenderer>();//Gets sprite renderer in child object 
        _inv = FindObjectOfType<Inventory>();//Finds the inventory in the scene

        _sprite = _sRend.sprite;//Stores the sprite from the sprite renderer
        _colour = _sRend.color;//Stores the colour from the sprite renderer 

        _trig.myTrigEntered += KeyPicked;//Add Key Picked function to the delegate in the trigger 
    }

    //Called when component is disabled 
    private void OnDisable()
    {
        _trig.myTrigEntered -= KeyPicked;//Removes Key Picked function from delegate
    }

    //Called when trigger is entereed
    void KeyPicked() 
    {
        if(_inv.AddToInv(gameObject, _sprite, _colour))//Checks if inventory has space and sends object details
        {
            _trig.enabled = false;//Turns trigger off
            _coll.enabled = false;//Turns collider off
            _sRend.enabled = false;//Turns renderer off
            _hasKey = true;//Sets has key to true
        }

    }

    //Is called when key is used
    public bool UseKey() 
    {
        if(_hasKey)//Checks if key has been acquired 
        {
            _numUses--;// Minuses one from number of uses
            if (_numUses <= 0)//If number of uses is zero or less 
            {
                _hasKey = false;//Has key is set to false
                _inv.RemoveFromInv(gameObject);//Key is removed from the inventory
            }
            return true;//Returns true as key is used
        }
        return false;//Returns false as key is not used
    }
}
