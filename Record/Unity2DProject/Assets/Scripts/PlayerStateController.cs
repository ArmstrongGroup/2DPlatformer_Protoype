using UnityEngine;
using Cinemachine;

public class PlayerStateController : MonoBehaviour
{
    //Private variables
    bool _pDead;//Store state of player dead

    //Reference variables
    PlayerController _playerCont;//Stores reference to player controller
    CharacterMovement _cMove;//Stores reference to movement
    CharacterJump _cJump;//Stores reference to jump
    CollisionChecks _collisionsCheck;//Stores reference to collision checks
    Rigidbody2D _rb;//Stores reference to rigidbody
    Collider2D _collider;//Stores reference to collider
    CinemachineVirtualCamera _vCam;//Stores reference to virtual camera
    PlayerShoot _pShoot;//Stores reference to player shoot
    GameManagment _gameManagment;//Stores reference to game manager

    // Start is called before the first frame update
    void Start()
    {   
        _playerCont = GetComponent<PlayerController>();//Populates player controller variable (Inputs)
        _cMove = GetComponent<CharacterMovement>();//Populates characer movement variable (x motion of the player)
        _cJump = GetComponent<CharacterJump>();//Populates characer jump variable (jumping)
        _collisionsCheck = GetComponent<CollisionChecks>();//Populates collision checks variable (blocking)
        _rb = GetComponent<Rigidbody2D>();//Populates rigidbody variable (physics / motion)
        _collider = GetComponent<Collider2D>();//Populates collider variable (collisions)
        _vCam = FindObjectOfType<CinemachineVirtualCamera>();//Populates reference to virtual camera
        _pShoot = GetComponent<PlayerShoot>();//Populates reference to player shoot
        _gameManagment = FindObjectOfType<GameManagment>();//Populates reference to Game Manager
    }

    // Update is called once per frame
    public void PlayerUpdate()
    {
        if (!_pDead)//Checks that player is not dead 
        {
            _playerCont.InputUpdate();//Runs player inputs
            _cMove.MoveUpdate();//Runs player movement
            _cJump.JumpUpdate();//Runs Jumping
            _pShoot.ShootUpdate();//Runs Shooting
            _collisionsCheck.CollisionUpdate();//Runs collision checks
        }
    }

    //Public function that enables the killing of the player
    public void Death() 
    {
        _pDead = true;//Sets death state to true
        _rb.velocity = Vector3.zero;//Stops motion
        _rb.simulated = false;//Stops falling from gravity
        _collider.enabled = false;//Stops collisions with other objects
        _vCam.enabled = false;//Stops camera following player
        _gameManagment.SetState(GameManagment.GameState.playerDied);
        Invoke("DeathAnim", 1f);//Invokes the death animation with a delay
    }

    //Runs death animation/motion
    void DeathAnim() 
    {
        _rb.simulated = true;//Turns gravity back on
        _rb.AddForce(transform.up * 7.5f, ForceMode2D.Impulse);//Jumps player up into the air
        Invoke("ResetGame", 2f);//Invokes the reset function with 2 second delay
    }

    //Reset function
    void ResetGame() 
    {
        _gameManagment.SetState(GameManagment.GameState.reset);//Sets game state in game manager to reset
        _rb.velocity = Vector3.zero;//Stops all motion on this object
        _rb.simulated = false;//Stops physics simulation
    }
}
