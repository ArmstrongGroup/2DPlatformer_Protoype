using UnityEngine;

public class Projectile : MonoBehaviour
{
    //Editable in Inspector
    [SerializeField]
    float _force = 10;//Force projectile is thrown

    //References
    Rigidbody2D _rb;//Stores reference to rigidbody (physics)

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();//Populates rigidbody variable
        _rb.AddForce(transform.right * _force, ForceMode2D.Impulse);//Applies force so projectile moves right by default (in local space)
        Destroy(gameObject, 5);//Destroys this projectile after 5 seconds
    }

    private void OnCollisionEnter2D() //Is called on collision with any other object
    {
        Destroy(gameObject);//Destroys self (this projectile)
    }
}
