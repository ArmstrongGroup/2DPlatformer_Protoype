using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    //Editable in inspector
    [SerializeField]
    List<Image> _imageSlots;//Image slots for inventory
    [SerializeField]
    List<GameObject> _inventorySlots;//Inventory itself (Game Objects)

    //Called to add to inventory
    public bool AddToInv(GameObject obj, Sprite pic, Color colour)//Takes information needed for inventory 
    {
        for (int i = 0; i < _inventorySlots.Count; i++)//Loops through all inventory slots 
        {
            if (_inventorySlots[i] == null)//Checks for an empty slot 
            {
                //Adds object to empty slot
                _inventorySlots[i] = obj;//Adds object to inventory
                _imageSlots[i].sprite = pic;//Adds pic to image slot
                _imageSlots[i].color = colour;//Sets colour of image
                return true;//Returns true as object is added to inventory
            }
        }
        return false;//Returns false as inventory is full
    }

    //Called to remove object from inventory
    public void RemoveFromInv(GameObject obj)//Takes in object being removed 
    {
        for (int i = 0; i < _inventorySlots.Count; i++)//Loops through the inventory
        {
            if (_inventorySlots[i] == obj)//Checks if object is in inventory
            {
                //Removes object when found
                _inventorySlots[i] = null;//Sets inventory slot to empty
                _imageSlots[i].sprite = null;//Sets inventory image slot to empty
                _imageSlots[i].color = new Color(1,1,1,0);//Sets inventory image colour to transparent

                InventoryReset();//Re orders inventory
            }
        }
    }

    //Pushes empty slots to the bottom of inventory
    void InventoryReset() 
    {
        for (int i = 0; i < _inventorySlots.Count; i++)//Loops throug all slots
        {
            if (_inventorySlots[i] == null)//Checks if slot is empty
            {
                if (i != _inventorySlots.Count - 1)//Checks the slot is not the last slot 
                {
                    _inventorySlots[i] = _inventorySlots[i+1];//Moves item from slot further down to this one
                    _imageSlots[i].sprite = _imageSlots[i+1].sprite;//Moves image from slot further down to this one
                    _imageSlots[i].color = _imageSlots[i+1].color;//Moves colour from slot further down to this one

                    _inventorySlots[i + 1] = null;//Sets this slot to empty
                    _imageSlots[i + 1].sprite = null;// Sets inventory image slot to empty
                    _imageSlots[i + 1].color = new Color(1, 1, 1, 0);//Sets inventory image colour to transparent
                }
            }
        }
    }

}
