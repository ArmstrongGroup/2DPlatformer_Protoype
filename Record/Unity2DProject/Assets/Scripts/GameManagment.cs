using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using static TMPro.SpriteAssetUtilities.TexturePacker_JsonArray;

public class GameManagment : MonoBehaviour
{
    //Public Game State to store state of the game
    public enum GameState {menu, inPlay, playerDied, playerDead, reset }; //Defines type
    [HideInInspector]
    public GameState _gState = GameState.inPlay; //Instance of newly defined type

    //Private lists
    List<EnemyStateController> _enemies = new List<EnemyStateController>(); //To store all enemy controllers in the scene
    List<MovingPlatform> _movePlatforms = new List<MovingPlatform>(); //To store all moving platforms in the scene

    //Private reference to the player state controller
    PlayerStateController _pStateCon;
    //Private reference to the game object of the parent in the death UI hierarchy
    GameObject _deathUI;

    // Start is called before the first frame update
    void Start()
    {   
        _pStateCon = FindObjectOfType<PlayerStateController>(); //Populates player state controller variable with the only one in the scene
        _deathUI = GameObject.Find("DeathUI");//Populates death UI variable with the only one in the scene
        _deathUI.SetActive(false);//Turns the death UI off

        Cursor.visible = false;//Hides the mouse cursor
        Cursor.lockState = CursorLockMode.Locked;//locks mouse cursor to the centre of the screen

        FindEnemies();//Finds the enemies
        FindPlatforms();//Finds the platforms
    }

    private void FindEnemies()
    {
        GameObject[] enems = GameObject.FindGameObjectsWithTag("Enemy");//Finds all game objects tagged enemy and stores them in a temporary array

        foreach (GameObject obj in enems)//Looks through array assigns each game objects enemy state component to the enemies list.
        { 
            _enemies.Add(obj.GetComponent<EnemyStateController>());
        }
    }

    private void FindPlatforms()
    {
        GameObject[] platforms = GameObject.FindGameObjectsWithTag("MovingPlatform");//Finds all game objects tagged moving platform and stores them in a temporary array

        foreach (GameObject obj in platforms)
        {
            _movePlatforms.Add(obj.GetComponent<MovingPlatform>());//Looks through array assigns each game objects moving platform component to the move plats list.
        }
    }


    // Update is called once per frame
    void Update()
    {
        StateMachine();//Runs state machine function
    }

    void StateMachine() 
    {
        switch(_gState)//Checks what the game state is 
        {
            case GameState.inPlay: //Runs in in play state
                RunPlatforms();//Runs the moving platforms
                _pStateCon.PlayerUpdate();//Runs the player
                RunEnemies();//Runs the enemies
                break;
            case GameState.playerDied://Runs when in player died state
                StopEnemies();//Stop all active enemies from moving
                StopPlatforms();//Stops all platforms from moving
                _gState = GameState.playerDead;//Sets game state to player dead
                break;
            case GameState.playerDead://Runs when in player dead state
                //No functionality, this state is there to prevent behaviours from running
                break;
            case GameState.reset://Runs when in reset state
                _deathUI.SetActive(true);//Turns death UI on
                Cursor.visible = true;//Makes mouse cursor visable
                Cursor.lockState = CursorLockMode.Confined;//Confines mouse cursor to game window
                _gState = GameState.menu;//Sets game state to menu

                break;
            case GameState.menu://Runs when in menu state
                //No functionality, this state is there to prevent behaviours from running
                break;
            default://Runs if other state is used
                Debug.LogError("Invalid Game State");//Logs an error to the console as this should not happen
                break;

        }
    }

    void RunPlatforms()//Moves the platforms
    {
        if (_movePlatforms != null)//Checks there are platforms to run
        {
            for (int i = 0; i < _movePlatforms.Count; i++)//Loops through all platforms every frame
            {
                if (_movePlatforms[i] == null)//Checks if this index IS null
                {
                    _movePlatforms.Remove(_movePlatforms[i]);//If null, it is removed to prevent errors
                }
                else //If NOT null
                {
                    _movePlatforms[i].PlatformUpdate();//Platform behaviour is run (movement)
                }
            }
        }
    }

    void StopPlatforms()//Stops the platforms
    {
        if (_movePlatforms != null)//Checks there are platforms to stop
        {
            for (int i = 0; i < _movePlatforms.Count; i++)// Loops through all platforms every frame
            {
                if (_movePlatforms[i] == null)//Checks if this index IS null
                {
                    _movePlatforms.Remove(_movePlatforms[i]);//If null, it is removed to prevent errors
                }
                else//If NOT null
                {
                    _movePlatforms[i].Stop();//Platform behaviour is stopped (movement)
                }
            }
        }
    }

    void RunEnemies() //Moves the enemies
    {
        if(_enemies != null) //Checks there are enemies to run
        {
            for (int i = 0; i < _enemies.Count; i++) //Loops through all enemies every frame
            {
                if (_enemies[i] == null)//Checks if this index IS null
                {
                    _enemies.Remove(_enemies[i]);//If null, it is removed to prevent errors
                }
                else //If NOT null
                {
                    _enemies[i].EnemyUpdate();//Enemy behaviour is run (movement)
                }
            }
        }
    }

    void StopEnemies() //Stops the enemies
    {
        if (_enemies != null)//Checks there are enemies to stop
        {
            for (int i = 0; i < _enemies.Count; i++)//Loops through all enemies every frame
            {
                if (_enemies[i] == null)//Checks if this index IS null
                {
                    _enemies.Remove(_enemies[i]);//If null, it is removed to prevent errors
                }
                else//If NOT null
                {
                    _enemies[i].Stop();//Enemy behaviour is stopped (movement)
                }
            }
        }
    }

    //Public function that enables the setting of the game state from elsewhere
    public void SetState(GameState state) 
    {
        _gState = state;
    }
}
