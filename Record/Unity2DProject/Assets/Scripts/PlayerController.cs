using UnityEngine;

public class PlayerController : MonoBehaviour
{
    float _inputX = 0;//Stores input in x from input manager
    public float InputX {get { return _inputX; } }//Allows input x to be readable

    bool _jump;//Stores jump from input manager
    public bool Jump { get { return _jump; } }//Allows jump to be readable

    bool _fire;//Stores fire from input manager
    public bool Fire { get { return _fire; } }//Allows fire to be readable

    // Update is called once per frame
    public void InputUpdate()
    {
        SetXInput();//Runs updating of input x
        SetJump();//Runs updating of jump
        SetFire();//Runs updating of fire
    }

    void SetXInput() 
    {
        _inputX = Input.GetAxisRaw("Horizontal");//Stets input x to Horizontal raw axis in input manager
    }

    void SetJump() 
    {
        _jump = Input.GetButtonDown("Jump");//Stets jump to jump button in input manager
    }

    void SetFire() 
    {
        _fire = Input.GetButtonDown("Fire1");//Sets fire using Fire1 in input manager
    }
}
