using UnityEngine;

public class CollisionChecks : MonoBehaviour
{
    //Editable in inspector
    [SerializeField]
    LayerMask _collisionMask;//Stores layer mask to limit collisions assessed based upon layers
    [SerializeField]
    Vector2 _collisionSize = new Vector2(0.5f, 1.0f);//Stores size of the character collider (must be set to size of game object's bounding box
    [SerializeField]
    [Range(0.0f, 1.0f)]
    float _amountVelAddedByPlatforms = 0.75f;

    bool _onGround;//Stores state of character on ground
    public bool OnGround { get { return _onGround; }}//Enables on ground to be readable

    bool _leftBlock;//Stores state of character left block
    public bool LeftBlock {  get { return _leftBlock; } }//Enables left block to be readable

    bool _rightBlock;//Stores state of character right block
    public bool RightBlock { get { return _rightBlock; } }//Enables right block to be readable

    RaycastHit2D[] _hitters;//Array storing every hit from surrounding game objects
    float _midpoint;//Stores difference in y of the game object's pivot point and the centre point
    Rigidbody2D _rb;//Stores reference to rigidbody on the player
    CharacterJump _cJump;//Stores reference to jump component on the player

    Rigidbody2D _platRB;//Stores reference to rigidbody on the platform
    Vector2 _addVelocity;//Stores velocity to add to the player when on moving platform
    bool _preventYVelocityApp;//Stores whether y velocity should be applied

    enum PlatformState {noPlatform, onPlatform, leftPlatform, jumpOff };//Declares type to store state of player in relation to a moving platform
    PlatformState _platState = PlatformState.noPlatform;//Declares variable of the above type

    private void OnDisable()
    {
        _cJump.myJump -= ReleasePlayer;//Removes Release Player function from delegate inside of the Jump component when this comopenent is disabled
    }


    // Start is called before the first frame update
    void Start()
    {
        _midpoint = _collisionSize.y / 2;//Sets midpoint
        Vector2 _tenPercent = new Vector2(_collisionSize.x / 10, _collisionSize.y / 10);//Stores value of 10% of collision size
        _collisionSize = _collisionSize + _tenPercent;//Increases collision size by 10%
        _rb = GetComponent<Rigidbody2D>();//Populates the rigidbody variable to the one on the player
        _cJump = GetComponent<CharacterJump>();//Populates the jump variable to the one on the player
        _cJump.myJump += ReleasePlayer;//Adds Release Player function to the delegate inside the Jump component
    }

    // Update is called once per frame
    public void CollisionUpdate()
    {
        Collisions();//Runs collisions
        VelocityUpdate();//Runs velocity updating
    }

    void Collisions() 
    {
        Vector2 origin = new Vector2(transform.position.x, transform.position.y + _midpoint);//Calculates the center point of the game object in world space
        _hitters = Physics2D.BoxCastAll(origin, _collisionSize, 0f, Vector2.zero, 0f, _collisionMask, -0.1f, 0.1f);//Gets objects colliding with this game object via Box Cast
        //Objects are sored in an array

        _onGround = false;//Sets on ground to false every frame
        _leftBlock = false;//Sets left block to false every frame
        _rightBlock = false;//Sets right block to false every frame

        if (_hitters.Length > 0)//Only runs if an object is hitting this game object 
        {
            for (int i = 0; i < _hitters.Length; i++)//Loops through every object hit every frame 
            {
                //Runs when defualt layer is hit
                if (_hitters[i].collider.gameObject.layer == 0 || _hitters[i].collider.gameObject.layer == 9)
                {
                    if (!_onGround)//Only runs if on ground is false 
                    {
                        if (_hitters[i].point.y <= transform.position.y)//Checks if hit point is below the player
                        {
                            _onGround = true;//Sets on ground to true
                        }
                    }

                    if (!_rightBlock)//Only runs if right block is false 
                    {
                        if (_hitters[i].point.x < transform.position.x)//Checks if hit is on left
                        {
                            //Hit is disregarded
                        }
                        //Checks if hit is not on the bottom of the player or the top of the player
                        else if (_hitters[i].point.y >= transform.position.y
                            && _hitters[i].point.y < (transform.position.y + (_midpoint * 2)))
                        {
                            _rightBlock = true;//Sets right block to true
                        }
                    }

                    if (!_leftBlock)//Only runs if left block is false 
                    {
                        if (_hitters[i].point.x > transform.position.x)//Checks if hit is on right
                        {
                            //Hit is disregarded
                        }
                        //Checks if hit is not on the bottom of the player or the top of the player
                        else if (_hitters[i].point.y >= transform.position.y
                            && _hitters[i].point.y < (transform.position.y + (_midpoint * 2)))
                        {
                            _leftBlock = true;//Sets left block to true
                        }
                    }
                }

                //Runs when hazard is hit
                else if (_hitters[i].collider.gameObject.layer == 6)
                {
                    KillPlayer();
                }
                //Runs when enemy is hit
                else if (_hitters[i].collider.gameObject.layer == 7)
                {
                    //Checks if player has hit the side of the enemy
                    if (_hitters[i].collider.gameObject.transform.position.y > (transform.position.y - 0.4f))
                    {
                        KillPlayer();//Kills the player
                    }
                    else //Kills the enemy
                    {
                        _hitters[i].collider.gameObject.BroadcastMessage("Die");//Sets die functionality on enemy off
                        _rb.velocity = Vector3.zero;//Stops motion
                        _rb.AddForce(transform.up * 7.5f, ForceMode2D.Impulse);//Jumps the player upwards
                    }
                }
                //Runs when moving platform is hit
                if (_hitters[i].collider.gameObject.layer == 9)
                {
                    //Is called if object hit is below the player and the player velocity is downwards or flat
                    if ((_hitters[i].collider.transform.position.y < transform.position.y) && (_rb.velocity.y <= 0))
                    {
                        if (_platRB == null)//Checks if the the variable storing the platform rigidbody is empty
                        {
                            
                            _platRB = _hitters[i].collider.GetComponent<Rigidbody2D>();//If empty, variable is populated
                        }
                        //If player is not on a platform or has not just jumped off
                        if (_platState != PlatformState.jumpOff || _platState != PlatformState.onPlatform)
                        {
                            _platState = PlatformState.onPlatform;//Platform state is set to play being on the platform
                        }
                    }
                }
            }
            if (NoPlatform())//Check id the player is not on a platform
            {
                if (_platState == PlatformState.onPlatform)//Checks if state is set to on a platform 
                {
                    if (_platRB.velocity.y >= 0 || _platRB.transform.position.y > transform.position.y)
                    {
                        _platState = PlatformState.leftPlatform;//Changes state to left pltform
                    }
                }
                else
                {
                    _platState = PlatformState.noPlatform;//Sets state to no platform
                    _platRB = null;//Clears platform rigidbody variable ready for next platform
                }
            }
        }
        else //Called when there are no collisions
        {
            if (_platState == PlatformState.onPlatform)//Checks if state is set to on a platform 
            {
                if (_platRB.velocity.y >= 0 || _platRB.transform.position.y > transform.position.y)
                {
                    _platState = PlatformState.leftPlatform;//Changes state to left pltform
                }
            }
            else if(_platState == PlatformState.noPlatform && _platRB != null)//Checks if not on platform but still has platform rigidbody assigned 
            {
                _platRB = null;//Clears platform rigidbody variable ready for next platform
            }
        }
    }

    bool NoPlatform()//Returns true if no pltform and false if on platform
    {
        foreach (RaycastHit2D hit in _hitters) //Loops through all hits
        {
            if (hit.collider.gameObject.layer == 9) //Check if in the moving platform layer
            {
                return false; //Returns false if platform is detected
            }
        }
        return true;//Returns true if NO platform is detected
    }

    void VelocityUpdate() //Updates the velocity
    {
        if (_platState == PlatformState.noPlatform)//Checks if no platform is in contact with the player
        {
            _addVelocity = Vector2.zero;//Sets the velocity to add to the players velocity to zero as no platform is in contact
        }
        else //The player is in contact with the platform or is leaving the platform
        {
            if ((_platState == PlatformState.leftPlatform || _platState == PlatformState.jumpOff) && _platRB != null)//Check whether player is leaving the platform and platfrom rigidbody is still assigned
            {
                if (_preventYVelocityApp == false) //Effectively checks if player has jumped off the platform
                {
                    //Sets velocity of player in y be the jump velocity plus a percentage of the platforms y velocity
                    _rb.velocity = new Vector2(_rb.velocity.x, _rb.velocity.y + (_platRB.velocity.y * _amountVelAddedByPlatforms));
                    _preventYVelocityApp = true;//Sets to true so the above line will not be called again until the player jumps off a platform
                }

                //Sets the about of x velocity to add to the player to be a percentage of the x velocity of the platform
                _addVelocity.x = _platRB.velocity.x * _amountVelAddedByPlatforms;
                _platRB = null;//Sets platform rigidbody to null, so next collision can add a new one if needed and prevents this section from being called multiple frames
            }
            else if (_platState == PlatformState.onPlatform) //If on a platform
            {
                _addVelocity.x = _platRB.velocity.x;//Sets the add velocity in x to be the same as the platform's x velocity (so player moves with the platfom)
                _rb.velocity = new Vector2(_rb.velocity.x, _platRB.velocity.y);//Sets the y velocity of the player to be the same as the platform
            }

            if (!OnGround && _platRB != null) //If player is no longer grounded but has been on a platform (i.e. they have walked off the side)
            { 
                if (_platRB.velocity.y >= 0 || _platRB.transform.position.y > transform.position.y)
                {
                    _platState = PlatformState.leftPlatform;//Sets state to left platform 
                    _preventYVelocityApp = true;//Sets prevent y velocity application to true to not jump player in the air and let them fall
                }
            }
        }
        _rb.velocity = _rb.velocity + _addVelocity;//Adds add velocity to current player velocity
    }

    void ReleasePlayer()//Subscribed to delegate in jump component
    {
        _preventYVelocityApp = false;//Sets prevent y velocity application to false to enable jump upwards

        if (_platState == PlatformState.onPlatform)//If player is on a platform, then sets state to jumped off 
        {
            _platState = PlatformState.jumpOff;//Sets state
        }
    }

    void KillPlayer() 
    {
        BroadcastMessage("Death");//Sets of death functionality on other scripts
    }
}
