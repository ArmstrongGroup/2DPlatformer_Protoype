using UnityEngine;

public class EnemyStateController : MonoBehaviour
{
    //Private variables
    bool _eDead; //Stores state of enemy death
    bool _falling; //Stores state of enemy falling

    //Reference variables
    EnemyMovement _eMove;//Stores reference to Enemy Movement
    EnemyCollisionChecks _eCollisionChecks;//Stores reference to Enemy Collision Checks
    Rigidbody2D _rb;//Stores reference to rigidbody
    Collider2D _col;//Stores refernce to collider

    // Start is called before the first frame update
    void Start()
    {
        _eMove = GetComponent<EnemyMovement>();//Populates ref to enemy movement (motion)
        _eCollisionChecks = GetComponent<EnemyCollisionChecks>();//Populates ref to enemy collision checks (checks blocking / grounding)
        _rb = GetComponent<Rigidbody2D>();//Populates ref to rigidbody (physics)
        _col = GetComponent<Collider2D>();//Populates ref to collider (collisions)
    }

    // Update is called once per frame
    public void EnemyUpdate()
    {
        if (!_eDead)//Checks enemy death state (runs if alive) 
        {
            _eMove.MoveUpdate();//Runs motion
            _eCollisionChecks.CollisionUpdate();//Runs collision checks
            FallCheck();//Runs Fall Check
        }
    }
    //Public function that sets enemy to death state
    public void Die() 
    {
        _eDead = true;//Sets death state to true
        _col.enabled = false;//Turns off collision
        _rb.velocity = Vector3.zero;//Stops movement
        _rb.AddForce(transform.up * 5, ForceMode2D.Impulse);//Makes the enemy jump up
        Invoke("Death", 2);//Invokes enemy destruction
    }

    //Destroys this enemy
    void Death() 
    {
        Destroy(gameObject);//Destroys this game object
    }

    //Checks if this enemy is falling
    void FallCheck() 
    {
        if(!_eCollisionChecks.OnGround && !_falling) //If enemy IS falling but NOT flagged as such 
        {
            _falling = true;//Sets falling state to true
            Invoke("Death", 2);//Invokes enemy destruction
        }
        if (_eCollisionChecks.OnGround && _falling)//If enemy is NOT falling but IS flagged as such 
        {
            _falling = false;//Sets falling state to false
            CancelInvoke("Death");//Stops this enemy from being destroyed
        }
    }

    //Public function to stop enemy movement
    public void Stop()
    {
        _rb.velocity = Vector3.zero;//Sets velocity to zero
        _rb.simulated = false;//Stop the physics simulation 
        CancelInvoke();//Cancels any invoked functions on this instance
    }
}
