using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    //Editable in Inspector
    [SerializeField]
    GameObject _bullet;//Refernce to projectile (populated from project window)
    //Private variables
    int _shootDir = 1;//Stores direction to shoot in
    Vector3 _projectileRot = Vector3.zero;//Stores target rotation to orientate projectile to
    //References
    PlayerController _pCont;//Stores reference to player controller (inputs)
    //Properties
    bool _hasGun;//Private variable storing state of whether the player has the gun
    public bool HasGun {  get { return _hasGun; }  set { _hasGun = value; } }//Property allowing has gun to be both read and writable

    // Start is called before the first frame update
    void Start()
    {
        _pCont = GetComponent<PlayerController>();//Populates player controller vaiable
        Physics2D.IgnoreLayerCollision(3, 8);//Ignores collisions between bullets and the player
    }

    // Shoot Update is called once per frame from player state controller
    public void ShootUpdate()
    {
        if (_hasGun)//Only runs was gun is aquired
        {
            UpdateDirection();//Updates shoot direction
            Shoot();//Checks for shot and spawns bullet
        }
    }

    void UpdateDirection()
    {
        if (_pCont.InputX > 0)//If player hits move right input
        {
            _shootDir = 1;//Sets shoot direction to be positive x
            _projectileRot = Vector3.zero;//Sets projectile target rotation to (0,0,0)
        }
        else if (_pCont.InputX < 0)//If player hits move left input 
        {
            _shootDir = -1;//Sets shoot direction to be negative x
            _projectileRot = new Vector3(0,0,180);//Sets projectile target rotation to (0,0,180)
        }
    }

    void Shoot() 
    {
        if (_pCont.Fire)//Checks for fire input 
        {
            //Creates a copy of bullet at shoot position the same side as the direction of motion and matches the rotation to the stored value in projectile rot
            Instantiate(_bullet, transform.position + new Vector3(0.35f * _shootDir, 0.5f, 0), Quaternion.Euler(_projectileRot));
        }
    }
}
