using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathMenu : MonoBehaviour
{
    //Public function to re-load the current scene 
    public void ReLoadScene() 
    {
        SceneManager.LoadScene(1);
    }
    //Public function to quit to the main menu scene
    public void QuitToMenu() 
    {
        SceneManager.LoadScene(0);
    }
}
