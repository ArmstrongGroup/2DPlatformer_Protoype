using UnityEngine;

public class DoorScript : MonoBehaviour
{
    //Editable in Inspector
    [SerializeField] 
    GameObject _door;//Reference to Door (assign in editor)
    [SerializeField]
    KeyScript _key;//Reference to Key (assign in editor)

    TriggerScript _trig;//Reference to trigger
    Animation _anim;//Reference to animation
    bool _doorOpen;//Stores state of the door

    // Start is called before the first frame update
    void Start()
    {
        _trig = GetComponentInChildren<TriggerScript>();//Finds trigger script in child object
        _anim = _door.GetComponent<Animation>();//Gets the animation component of the door
        _trig.myTrigEntered += MoveDoor;//Adds Move Door function to delegate in trigger script
        SetDoorColour();//Sets colour of the door
    }

    //Called on this script being disabled 
    private void OnDisable()
    {
        _trig.myTrigEntered -= MoveDoor;//Removes Move Door function from delegate in event od this component being disabled
    }

    //Moves door
    void MoveDoor() 
    {
        if (!_doorOpen) //Checks state of door (false means closed so will then check to open)
        {
            if (_key != null)//Checks if door needs key. Fulfilled if key needed 
            {
                if (_key.UseKey())//Checks if key can be used
                {
                    _doorOpen = true;//Sets door state to open 
                    _anim.Play();//Plays door animation
                }
                else //Key still needs to be acquired
                {
                    Debug.Log("Key Needed!");
                }
            }
            else//Door does not recquire key 
            {
                _anim.Play();//Plays animation
                _doorOpen = true;//Sets door state to open
            }
        }
    }

    //Sets door colour to same as key
    void SetDoorColour() 
    {
        //Checks if key is assigned
        if(_key != null) 
        {
            //Sets door colour to the same as the key's colour
            _door.GetComponent<SpriteRenderer>().color = _key.GetComponentInChildren<SpriteRenderer>().color;
        }
    }
}
