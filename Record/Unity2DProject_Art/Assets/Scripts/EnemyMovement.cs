using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    //Editable in Inspector
    [SerializeField]
    float _moveSpeed = 1;//Movement speed
    [SerializeField]
    bool _moveLeftFirst = false;//Stores direction to move first

    //Reference Variables
    Rigidbody2D _rb;//Stores rigidbody
    EnemyCollisionChecks _enemyColl;//Stores collision checks

    //Private variables
    Vector2 _velocity;//Stores velocity of enemy
    int _dir = 1;//Stores x direction of enemy motion

    // Awake is called before start
    void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();//Poplates rigidbody variable (physics)
        _enemyColl = GetComponent<EnemyCollisionChecks>();//Populates collision checks variable (block checking)
    }

    // Start is called before the first frame update
    void Start()
    {
        if (_moveLeftFirst)//Checks if enemy should move left first
        {
            _dir = -1;//Sets direction to left (negative x)
        }
        else 
        {
            _dir = 1;//Sets direction to right (positive x)
        }
    }

    // Public Update is called once per frame from another script
    public void MoveUpdate()
    {
        Movement();//Calls movement
    }

    void Movement() 
    {
        _velocity.y =_rb.velocity.y;//Sets y value of velocity
        _velocity.x = _moveSpeed * _dir;//Sets x value of velocity

        if (_enemyColl.LeftBlock) //Checks if left is blocked
        {
            _dir = 1;//Sets direction to move right
        }
        else if(_enemyColl.RightBlock) //Checks if right is blocked
        {
            _dir = -1;//Sets direction to move left
        }

        _rb.velocity = _velocity;//Applies velocity to the rigidbody (this makes the movement happen)
    }
}
