using UnityEngine;

public class ShootingPickup : MonoBehaviour
{
    //References
    TriggerScript _trigger;//Stores reference to trigger script
    PlayerShoot _pShoot;//Stores reference to player shoot
    SpriteRenderer _sRend;//Stores reference to sprite renderer

    // Start is called before the first frame update
    void Start()
    {
        _trigger = GetComponentInChildren<TriggerScript>();//Populates reference to trigger script (finds component on child game object)
        _pShoot = FindObjectOfType<PlayerShoot>();//Populates reference to player shoot (finds component on object in scene, of which there is only one)
        _sRend = GetComponent<SpriteRenderer>();//Populates reference to sprite renderer (on this game object)

        _trigger.myTrigEntered += PickedUp;//Subscribes PickedUp funtion to delegate in trigger script
    }

    private void OnDisable()
    {
        _trigger.myTrigEntered -= PickedUp;//Unsubscribes PickedUp funtion from delegate in trigger script
    }


    void PickedUp() 
    {
        _trigger.enabled = false;//Turns off trigger
        _sRend.enabled = false;//Turns off visual
        _pShoot.HasGun = true;//Changes state of player havin the gun
    }
}
