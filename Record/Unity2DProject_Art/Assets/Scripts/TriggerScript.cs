using UnityEngine;

public class TriggerScript : MonoBehaviour
{
    //Edit in inspector
    [SerializeField]
    string _effectedTag;//Tag that will trigger functionality

    bool _inTrigger = false;//Stores state of object in trigger

    //Delegate type created for enter
    public delegate void TriggerEntered();
    public TriggerEntered myTrigEntered;//Instance of delegate enter created

    //Delegate type created for exit
    public delegate void TriggerExited();
    public TriggerExited myTrigExited;//Instance of delegate exit created

    public bool InTrigger { get { return _inTrigger; }} //Enables inTrigger state to be readable

    //Runs on trigger entered
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(_effectedTag)) //Checks tag against effected tag field
        {
            _inTrigger = true;//Sets state on in trigger

            if (myTrigEntered != null)//Checks whether any functions are subscribed to the delegate 
            {
                myTrigEntered();//If has subscribers, they are run
            }

        }
    }
    //Runs on trigger exited
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag(_effectedTag)) //Checks tag against effected tag field
        {
            _inTrigger = false;//Sets state on in trigger

            if (myTrigExited != null)//Checks whether any functions are subscribed to the delegate
            {
                myTrigExited();//If has subscribers, they are run
            }
        }
    }
}
