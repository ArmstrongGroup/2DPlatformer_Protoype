using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    //Public function for loading first game play scene
    public void LoadScene() 
    {
        SceneManager.LoadScene(1);
    }

    //Public function for quitting apllication
    //Only works in build
    public void QuitGame()
    {
        Application.Quit();
    }
}
