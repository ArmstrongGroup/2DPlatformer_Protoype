using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    [SerializeField] float _effectMultiplier = 1;

    float _startX;
    GameObject _cam;


    // Start is called before the first frame update
    void Start()
    {
        _startX = transform.position.x;
        _cam = Camera.main.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        float dist = _cam.transform.position.x * _effectMultiplier;
        transform.position = new Vector3(_startX + dist, transform.position.y, transform.position.z);
    }
}
